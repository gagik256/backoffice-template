# Backoffice template

## System requirements
* Oracle JRE 11
    - java version "11.0.2"
 
* Connection to DB Postgres 12.3
    - Version 12.3
    - Environment variables for connection to DB
        - DB_URI
        - DB_USER
        - DB_PASSWORD
        
* Free ports
    - 8080: http (for request to /api/* )
      
### build project
./gradlew clean build

### Start tests
./gradlew test

### Start server
java -jar libs/backoffice-template-0.0.1-SNAPSHOT.jar

### LIVENESS CHECK
GET actuator/health
* In case of normal operation of the service, it will return 200 status code
* Otherwise it will return 503 status code

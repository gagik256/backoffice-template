INSERT INTO roles (name, description, created_at, updated_at) VALUES ('Admin', 'This is admin', '1595532977165', '1595532977165');

INSERT INTO permissions (name, description) VALUES ('USERS_CREATE', 'Create user permission');
INSERT INTO permissions (name, description) VALUES ('USERS_READ', 'Read user permission');
INSERT INTO permissions (name, description) VALUES ('USERS_DELETE', 'Delete user permission');
INSERT INTO permissions (name, description) VALUES ('USERS_UPDATE', 'Update user permission');
INSERT INTO permissions (name, description) VALUES ('ROLES_CREATE', 'Create role permission');
INSERT INTO permissions (name, description) VALUES ('ROLES_READ', 'Read role permission');
INSERT INTO permissions (name, description) VALUES ('ROLES_DELETE', 'Delete role permission');
INSERT INTO permissions (name, description) VALUES ('ROLES_UPDATE', 'Update role permission');

INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '1');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '2');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '3');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '4');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '5');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '6');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '7');
INSERT INTO role_to_permission (role_id, permission_id) VALUES ('1', '8');

INSERT INTO users (login, password, first_name, last_name, email, country, created_at, updated_at) VALUES ('admin', 'admin', 'Admin', 'Admin', 'test@gmail.com', 'USA', '1595532977165', '1595532977165');

INSERT INTO user_to_role (user_id, role_id) VALUES ('1', '1');
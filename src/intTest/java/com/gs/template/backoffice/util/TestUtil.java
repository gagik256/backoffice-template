package com.gs.template.backoffice.util;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class TestUtil {

    private static final Logger LOG = LoggerFactory.getLogger(TestUtil.class);

    public static String readJson(String requestPath) {
        String requestJson = "";
        try {
            requestJson = Resources.toString(Resources.getResource(requestPath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOG.error("Can not read JSON form file: " + requestPath, e);
        }
        return requestJson;
    }
}

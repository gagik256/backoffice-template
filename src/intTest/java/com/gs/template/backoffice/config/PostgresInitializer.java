package com.gs.template.backoffice.config;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.lifecycle.Startables;

import java.util.stream.Stream;

public class PostgresInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Container
    public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:13.3")
            .withPassword("test")
            .withUsername("test")
            .withDatabaseName("test");

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        Startables.deepStart(Stream.of(postgreSQLContainer)).join();
        applicationContext.getBeanFactory().registerSingleton("postgresContainer", postgreSQLContainer);
        applicationContext.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                postgreSQLContainer.stop();
            }
        });

        TestPropertyValues values = TestPropertyValues.of(
                "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                "spring.datasource.username=" + postgreSQLContainer.getUsername()
        );
        values.applyTo(applicationContext);
    }
}

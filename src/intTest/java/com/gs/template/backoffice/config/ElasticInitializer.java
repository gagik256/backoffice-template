package com.gs.template.backoffice.config;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.lifecycle.Startables;

import java.util.stream.Stream;

public class ElasticInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Container
    public static ElasticsearchContainer elasticsearchContainer = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:7.13.2");

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        Startables.deepStart(Stream.of(elasticsearchContainer)).join();
        applicationContext.getBeanFactory().registerSingleton("elasticsearchContainer", elasticsearchContainer);
        applicationContext.addApplicationListener(applicationEvent -> {
            if (applicationEvent instanceof ContextClosedEvent) {
                elasticsearchContainer.stop();
            }
        });

        TestPropertyValues values = TestPropertyValues.of(
                "spring.elasticsearch.rest.uris=" + elasticsearchContainer.getHttpHostAddress());
        values.applyTo(applicationContext);
    }
}

package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.util.TestUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTest extends BaseIntegrationTest {

    @Test
    @Order(1)
    @Sql(value = "classpath:controllers/user/db-init/test-users.sql")
    public void userListTest() {
        String requestJson = TestUtil.readJson("controllers/user/request/success/userListRequest.json");
        String responseJson = TestUtil.readJson("controllers/user/response/success/userListResponse.json");

        ResponseEntity<String> response = call(API_BASE + USERS + LIST, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDatesInArray());
    }

    @Test
    @Order(2)
    public void createUserTest() {
        String requestJson = TestUtil.readJson("controllers/user/request/success/userCreateRequest.json");
        String responseJson = TestUtil.readJson("controllers/user/response/success/userCreateResponse.json");

        ResponseEntity<String> response = call(API_BASE + USERS, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDates());
    }

    @Test
    @Order(3)
    public void userUpdateTest() {
        String requestJson = TestUtil.readJson("controllers/user/request/success/userUpdateRequest.json");
        String responseJson = TestUtil.readJson("controllers/user/response/success/userUpdateResponse.json");

        ResponseEntity<String> response = call(API_BASE + USERS + "/7", HttpMethod.PUT, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDates());
    }

    @Test
    @Order(4)
    public void userDeleteTest() {
        ResponseEntity<String> response = call(API_BASE + USERS + "/1", HttpMethod.DELETE, StringUtils.EMPTY);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertNull(actualResponseJson);
    }

    @Test
    @Order(5)
    public void usersDeleteTest() {
        String requestJson = TestUtil.readJson("controllers/user/request/success/usersDeleteRequest.json");
        String responseJson = TestUtil.readJson("controllers/user/response/success/usersDeleteResponse.json");

        ResponseEntity<String> response = call(API_BASE + USERS + DELETE, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson);
    }
}

package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.util.TestUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RoleControllerTest extends BaseIntegrationTest {

    @Test
    @Order(1)
    @Sql(value = "classpath:controllers/role/db-init/test-roles.sql")
    public void roleListTest() {
        String requestJson = TestUtil.readJson("controllers/role/request/success/roleListRequest.json");
        String responseJson = TestUtil.readJson("controllers/role/response/success/roleListResponse.json");

        ResponseEntity<String> response = call(API_BASE + ROLES + LIST, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDatesInArray());
    }

    @Test
    @Order(2)
    public void createRoleTest() {
        String requestJson = TestUtil.readJson("controllers/role/request/success/roleCreateRequest.json");
        String responseJson = TestUtil.readJson("controllers/role/response/success/roleCreateResponse.json");

        ResponseEntity<String> response = call(API_BASE + ROLES, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDates());
    }

    @Test
    @Order(3)
    public void updateRoleTest() {
        String requestJson = TestUtil.readJson("controllers/role/request/success/roleUpdateRequest.json");
        String responseJson = TestUtil.readJson("controllers/role/response/success/roleUpdateResponse.json");

        ResponseEntity<String> response = call(API_BASE + ROLES + "/3", HttpMethod.PUT, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson, customMapperForDates());
    }

    @Test
    @Order(4)
    public void deleteRoleTest() {
        ResponseEntity<String> response = call(API_BASE + ROLES + "/3", HttpMethod.DELETE, StringUtils.EMPTY);
        HttpStatus responseStatusCode = response.getStatusCode();

        assertEquals(HttpStatus.OK, responseStatusCode);
    }

    @Test
    @Order(5)
    public void deleteRolesTest() {
        String requestJson = TestUtil.readJson("controllers/role/request/success/rolesDeleteRequest.json");
        String responseJson = TestUtil.readJson("controllers/role/response/success/rolesDeleteResponse.json");

        ResponseEntity<String> response = call(API_BASE + ROLES + DELETE, HttpMethod.POST, requestJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson);
    }
}

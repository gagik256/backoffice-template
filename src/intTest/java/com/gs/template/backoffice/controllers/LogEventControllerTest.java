package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.util.TestUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LogEventControllerTest extends BaseIntegrationTest {

    public static final String USER = "/user";
    public static final String ROLE = "/role";

    @Test
    @Order(1)
    public void userLogSuccessTest() {
        String requestUserCreationJson = TestUtil.readJson("controllers/logs/user/request/success/userCreateRequest.json");
        String requestUserUpdateJson = TestUtil.readJson("controllers/logs/user/request/success/userUpdateRequest.json");

        call(API_BASE + USERS, HttpMethod.POST, requestUserCreationJson);
        call(API_BASE + USERS + "/2", HttpMethod.PUT, requestUserUpdateJson);
        call(API_BASE + USERS + "/2", HttpMethod.DELETE, StringUtils.EMPTY);

        String requestUserLogJson = TestUtil.readJson("controllers/logs/user/request/success/userLogListRequest.json");
        String responseUserLogJson = TestUtil.readJson("controllers/logs/user/response/success/userLogCreateResponse.json");

        ResponseEntity<String> response = call(API_BASE + LOGS + USER + LIST, HttpMethod.POST, requestUserLogJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseUserLogJson, actualResponseJson, customMapperForDatesInArray());
    }

    @Test
    @Order(2)
    public void roleLogSuccessTest() {
        String requestRoleCreationJson = TestUtil.readJson("controllers/logs/role/request/success/roleCreateRequest.json");
        String requestRoleUpdateJson = TestUtil.readJson("controllers/logs/role/request/success/roleUpdateRequest.json");

        call(API_BASE + ROLES, HttpMethod.POST, requestRoleCreationJson);
        call(API_BASE + ROLES + "/2", HttpMethod.PUT, requestRoleUpdateJson);
        call(API_BASE + ROLES + "/2", HttpMethod.DELETE, StringUtils.EMPTY);

        String requestRoleLogJson = TestUtil.readJson("controllers/logs/role/request/success/roleLogListRequest.json");
        String responseRoleLogJson = TestUtil.readJson("controllers/logs/role/response/success/roleLogCreateResponse.json");

        ResponseEntity<String> response = call(API_BASE + LOGS + ROLE + LIST, HttpMethod.POST, requestRoleLogJson);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseRoleLogJson, actualResponseJson, customMapperForDatesInArray());
    }
}

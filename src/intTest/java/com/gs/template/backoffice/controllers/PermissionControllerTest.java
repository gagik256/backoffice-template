package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.util.TestUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PermissionControllerTest extends BaseIntegrationTest {

    @Test
    @Order(1)
    public void roleListTest() {
        String responseJson = TestUtil.readJson("controllers/permission/response/success/permissionListResponse.json");

        ResponseEntity<String> response = call(API_BASE + PERMISSIONS + LIST, HttpMethod.GET, StringUtils.EMPTY);
        HttpStatus responseStatusCode = response.getStatusCode();
        String actualResponseJson = response.getBody();

        assertEquals(HttpStatus.OK, responseStatusCode);
        assertJson(responseJson, actualResponseJson);
    }
}

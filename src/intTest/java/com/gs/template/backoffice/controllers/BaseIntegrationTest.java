package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.config.ElasticInitializer;
import com.gs.template.backoffice.config.PostgresInitializer;
import com.gs.template.backoffice.util.TestUtil;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.ArrayValueMatcher;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@Testcontainers
@DirtiesContext
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration(initializers = {PostgresInitializer.class, ElasticInitializer.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseIntegrationTest {

    private static final Logger LOG = LoggerFactory.getLogger(TestUtil.class);

    public static final String USERS = "/users";
    public static final String ROLES = "/roles";
    public static final String PERMISSIONS = "/permissions";
    public static final String LIST = "/list";
    public static final String LOGS = "/logs";
    public static final String DELETE = "/delete";
    public static final String API_BASE = "/api/v1";
    public static final String HOST = "http://localhost:";
    public static final String APPLICATION_JSON = "application/json";
    public static final String CREATED_AT = "createdAt";
    public static final String UPDATED_AT = "updatedAt";

    @LocalServerPort
    private int port;

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    public ResponseEntity<String> call(String uri, HttpMethod method, String request) {
        HttpHeaders headers = buildHeaders();
        return restTemplate.exchange(
                createURLWithPort(uri), method, new HttpEntity<>(request, headers), String.class);
    }

    public void assertJson(String expected, String actual, CustomComparator customComparator) {
        try {
            JSONAssert.assertEquals(expected, actual, customComparator);
        } catch (JSONException e) {
            LOG.error("Can not assert Jsons, something wrong with the json structure, " +
                    "expected: \n {} \n actual: \n {}", expected, actual, e);
            Assert.fail();
        }
    }

    public void assertJson(String expected, String actual) {
        try {
            JSONAssert.assertEquals(expected, actual, false);
        } catch (JSONException e) {
            LOG.error("Can not assert Jsons, something wrong with the json structure, " +
                    "expected: \n {} \n actual: \n {}", expected, actual, e);
            Assert.fail();
        }
    }

    protected CustomComparator customMapperForDates() {
        return new CustomComparator(JSONCompareMode.LENIENT,
                new Customization(CREATED_AT, (value1, value2) -> true),
                new Customization(UPDATED_AT, (value1, value2) -> true)
        );
    }

    protected CustomComparator customMapperForDatesInArray() {
        return new CustomComparator(
                JSONCompareMode.NON_EXTENSIBLE,
                new Customization("result",
                        new ArrayValueMatcher<>(new CustomComparator(
                                JSONCompareMode.NON_EXTENSIBLE,
                                new Customization("result[*]." + CREATED_AT, (o1, o2) -> true),
                                new Customization("result[*]." + UPDATED_AT, (o1, o2) -> true)
                        ))));
    }

    private HttpHeaders buildHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(CONTENT_TYPE, APPLICATION_JSON);
        return headers;
    }

    private String createURLWithPort(String uri) {
        return HOST + port + uri;
    }
}

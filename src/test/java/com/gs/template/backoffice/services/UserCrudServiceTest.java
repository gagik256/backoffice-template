package com.gs.template.backoffice.services;

import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.QUser;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.repositories.jpa.UserRepository;
import com.gs.template.backoffice.services.impl.UserCrudServiceImpl;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = UserCrudServiceImpl.class)
public class UserCrudServiceTest {

    @Autowired
    private UserCrudService userCrudService;

    @MockBean
    private UserRepository userRepository;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void saveOk() {
        User expected = TestUtil.getUser();
        when(userRepository.save(expected)).thenReturn(expected);
        User actual = userCrudService.save(expected);
        Assertions.assertEquals(expected, actual);
        verify(userRepository, times(1)).save(expected);
    }

    @Test
    public void findByIdOk() throws UserNotFoundException {
        User expected = TestUtil.getUser();
        when(userRepository.findOne(QUser.user.id.eq(expected.getId()))).thenReturn(Optional.of(expected));
        User actual = userCrudService.findById(expected.getId());
        Assertions.assertEquals(expected, actual);
        verify(userRepository, times(1)).findOne(QUser.user.id.eq(expected.getId()));
    }

    @Test
    public void findByIdError() {
        Long id = TestUtil.getLong();
        when(userRepository.findOne(QUser.user.id.eq(id))).thenReturn(Optional.empty());
        Assertions.assertThrows(UserNotFoundException.class,
                () -> userCrudService.findById(id));
        verify(userRepository, times(1)).findOne(QUser.user.id.eq(id));
    }

    @Test
    public void deleteOk() {
        User expected = TestUtil.getUser();
        userCrudService.delete(expected);
        verify(userRepository, times(1)).delete(expected);
    }

    @Test
    public void findByLoginOk() {
        User expected = TestUtil.getUser();
        when(userRepository.findOne(
                QUser.user.login.eq(expected.getLogin().trim())))
                .thenReturn(Optional.of(expected));
        Optional<User> optActual = userCrudService.findByLogin(expected.getLogin());
        Assertions.assertTrue(optActual.isPresent());
        Assertions.assertEquals(expected, optActual.get());
        verify(userRepository, times(1))
                .findOne(QUser.user.login.eq(expected.getLogin().trim()));
    }

    @Test
    public void findByLoginEmpty() {
        String login = TestUtil.getString();
        when(userRepository.findOne(QUser.user.login.eq(login.trim())))
                .thenReturn(Optional.empty());
        Optional<User> optActual = userCrudService.findByLogin(login);
        Assertions.assertTrue(optActual.isEmpty());
        verify(userRepository, times(1)).findOne(QUser.user.login.eq(login.trim()));
    }

    @Test
    public void findByEmailOk() {
        User expected = TestUtil.getUser();
        when(userRepository.findOne(QUser.user.email.eq(expected.getEmail().trim().toLowerCase())))
                .thenReturn(Optional.of(expected));
        Optional<User> optActual = userCrudService.findByEmail(expected.getEmail());
        Assertions.assertTrue(optActual.isPresent());
        Assertions.assertEquals(expected, optActual.get());
        verify(userRepository, times(1))
                .findOne(QUser.user.email.eq(expected.getEmail().trim().toLowerCase()));
    }

    @Test
    public void findByEmailEmpty() {
        String email = TestUtil.getString();
        when(userRepository.findOne(QUser.user.email.eq(email.trim().toLowerCase())))
                .thenReturn(Optional.empty());
        Optional<User> optActual = userCrudService.findByEmail(email);
        Assertions.assertTrue(optActual.isEmpty());
        verify(userRepository, times(1))
                .findOne(QUser.user.email.eq(email.trim().toLowerCase()));
    }

    @Test
    public void findByIdsOk() {
        User expected = TestUtil.getUser();
        Set<Long> ids = Set.of(expected.getId());
        when(userRepository.findAll(QUser.user.id.in(ids)))
                .thenReturn(Set.of(expected));
        Set<User> actualSet = userCrudService.findByIds(ids);
        Assertions.assertEquals(1, actualSet.size());
        Assertions.assertTrue(actualSet.contains(expected));
        verify(userRepository, times(1)).findAll(QUser.user.id.in(ids));
    }
}

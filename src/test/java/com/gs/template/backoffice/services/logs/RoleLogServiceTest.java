package com.gs.template.backoffice.services.logs;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.log.RoleLogDto;
import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import com.gs.template.backoffice.model.entities.elastic.log.RoleLog;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.repositories.elastic.log.ElasticDao;
import com.gs.template.backoffice.repositories.elastic.log.RoleLogElasticRepository;
import com.gs.template.backoffice.services.log.RoleLogService;
import com.gs.template.backoffice.services.log.impl.RoleLogServiceImpl;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static com.gs.template.backoffice.services.log.impl.RoleLogServiceImpl.ROLE_LOG_INDEX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = RoleLogServiceImpl.class)
class RoleLogServiceTest {

    @Autowired
    private RoleLogService roleLogService;

    @MockBean
    private RoleLogElasticRepository roleLogRepository;

    @MockBean
    private ElasticDao<RoleLog> elasticDao;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(roleLogRepository);
    }

    @Test
    public void logCreatingEventOk() {
        Role role = TestUtil.getRole();

        roleLogService.logCreatingEvent(role);

        verify(roleLogRepository, times(1)).save(any(RoleLog.class));
    }

    @Test
    public void logUpdatingEventOk() {
        Role role = TestUtil.getRole();

        roleLogService.logUpdatingEvent(role, role);

        verify(roleLogRepository, times(1)).save(any(RoleLog.class));
    }

    @Test
    public void logDeletingEventOk() {
        Role role = TestUtil.getRole();

        roleLogService.logDeletingEvent(role);

        verify(roleLogRepository, times(1)).save(any(RoleLog.class));
    }

    @Test
    public void getRolesLogs() {
        RoleLog roleLog = TestUtil.getRoleLog(LogEventType.CREATE);
        List<RoleLog> roleLogs = Collections.singletonList(roleLog);
        PageRequestDto pageRequestDto = TestUtil.getPageRequestDto(SortDto.Order.DESC, "" +
                "id", "id", "1", FilterDto.Operator.EQ);

        when(elasticDao.findByQuery(any(NativeSearchQuery.class), any(), eq(ROLE_LOG_INDEX))).thenReturn(roleLogs);

        PageResponseDto<RoleLogDto> rolesLogsResponse = roleLogService.getRolesLogs(pageRequestDto);

        assertNotNull(rolesLogsResponse);
        assertEquals(1, rolesLogsResponse.getTotal().getCount());

        verify(elasticDao, timeout(1)).findByQuery(any(NativeSearchQuery.class), any(), eq(ROLE_LOG_INDEX));
    }
}

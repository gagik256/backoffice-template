package com.gs.template.backoffice.services;

import com.querydsl.core.types.Predicate;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.repositories.jpa.PermissionRepository;
import com.gs.template.backoffice.services.impl.PermissionServiceImpl;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ContextConfiguration(classes = PermissionServiceImpl.class)
public class PermissionServiceTest {
    @Autowired
    private PermissionService permissionsService;
    @MockBean
    private PermissionRepository permissionRepository;
    private static final Sort PERM_DEFAULT_SORT = Sort.by(Sort.Direction.ASC, "name");

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(permissionRepository);
    }

    @Test
    public void getPermissionNamesOk() {
        Permission superAdmin = TestUtil.getPermission();
        List<Permission> permissions = List.of(TestUtil.getPermission(), superAdmin);
        when(permissionRepository.findAll(PERM_DEFAULT_SORT)).thenReturn(permissions);

        PageResponseDto<PermissionDto> permissionDtos = permissionsService.getPermissionsDto();

        assertNotNull(permissionDtos);
        Assertions.assertEquals(permissions.size(), permissionDtos.getTotal().getCount());
        verify(permissionRepository, times(1)).findAll(PERM_DEFAULT_SORT);
    }

    @Test
    public void findAllPermissionsByNamesOk() {
        List<Permission> permissions = new ArrayList<>();
        permissions.add(TestUtil.getPermission());
        when(permissionRepository.findAll(any(Predicate.class))).thenReturn(permissions);

        Set<Permission> allPermissionsByNames = permissionsService.findAllPermissionsByNames(Collections.singleton("test"));
        assertNotNull(allPermissionsByNames);

        verify(permissionRepository, times(1)).findAll(any(Predicate.class));
    }
}

package com.gs.template.backoffice.services;

import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.QRole;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.repositories.jpa.RoleRepository;
import com.gs.template.backoffice.services.impl.RoleCrudServiceImpl;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = RoleCrudServiceImpl.class)
public class RoleCrudServiceTest {
    @Autowired
    private RoleCrudService roleCrudService;
    @MockBean
    private RoleRepository roleRepository;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(roleRepository);
    }

    @Test
    public void findByNameOk() {
        Role expected = TestUtil.getRole();
        when(roleRepository.findOne(
                QRole.role.name.eq(expected.getName().trim())))
                .thenReturn(Optional.of(expected));
        Optional<Role> optActual = roleCrudService.findByName(expected.getName());
        Assertions.assertTrue(optActual.isPresent());
        Assertions.assertEquals(expected, optActual.get());
        verify(roleRepository, times(1))
                .findOne(QRole.role.name.eq(expected.getName().trim()));
    }

    @Test
    public void findByNameEmpty() {
        String name = TestUtil.getString();
        when(roleRepository.findOne(
                QRole.role.name.eq(name.trim())))
                .thenReturn(Optional.empty());
        Optional<Role> optActual = roleCrudService.findByName(name);
        Assertions.assertTrue(optActual.isEmpty());
        verify(roleRepository, times(1)).findOne(QRole.role.name.eq(name.trim()));
    }

    @Test
    public void saveOk() {
        Role expected = TestUtil.getRole();
        when(roleRepository.save(expected)).thenReturn(expected);
        Role actual = roleCrudService.save(expected);
        Assertions.assertEquals(expected, actual);
        verify(roleRepository, times(1)).save(expected);
    }

    @Test
    public void findByIdOk() throws RoleNotFoundException {
        Role expected = TestUtil.getRole();
        when(roleRepository.findById(expected.getId())).thenReturn(Optional.of(expected));
        Role actual = roleCrudService.findById(expected.getId());
        Assertions.assertEquals(expected, actual);
        verify(roleRepository, times(1)).findById(expected.getId());
    }

    @Test
    public void findByIdError() {
        Long id = TestUtil.getLong();
        when(roleRepository.findById(id)).thenReturn(Optional.empty());
        Assertions.assertThrows(RoleNotFoundException.class,
                () -> roleCrudService.findById(id));
        verify(roleRepository, times(1)).findById(id);
    }

    @Test
    public void deleteOk() {
        Role expected = TestUtil.getRole();
        roleCrudService.delete(expected);
        verify(roleRepository, times(1)).delete(expected);
    }
}


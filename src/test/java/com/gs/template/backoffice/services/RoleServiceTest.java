package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.role.CreateRoleDto;
import com.gs.template.backoffice.dto.role.RoleDto;
import com.gs.template.backoffice.dto.role.UpdateRoleDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.RoleAlreadyExistException;
import com.gs.template.backoffice.errors.api.RoleCreationException;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.model.api.Error;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.services.impl.RoleServiceImpl;
import com.gs.template.backoffice.services.log.RoleLogService;
import com.gs.template.backoffice.util.TestUtil;
import com.gs.template.backoffice.utils.SerializationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = RoleServiceImpl.class)
public class RoleServiceTest {
    @Autowired
    private RoleService roleService;
    @MockBean
    private UserCrudService userCrudService;
    @MockBean
    private RoleCrudService roleCrudService;
    @MockBean
    private PermissionService permissionService;
    @MockBean
    private RoleLogService roleLogService;
    @MockBean
    private SerializationUtil serializationUtil;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(permissionService, roleCrudService, userCrudService);
    }

    @Test
    public void createRoleOk() throws RoleAlreadyExistException, RoleCreationException {
        CreateRoleDto createRoleDto = TestUtil.getCreateRoleDto();
        Role expectedRole = TestUtil.getRole();
        when(roleCrudService.findByName(createRoleDto.getName()))
                .thenReturn(Optional.empty());
        when(roleCrudService.save(any(Role.class))).thenReturn(expectedRole);
        when(permissionService.findAllPermissionsByNames(createRoleDto.getPermissions()))
                .thenReturn(Set.of(TestUtil.getPermission()));
        RoleDto actualRoleDto = roleService.createRole(createRoleDto);
        assertNotNull(actualRoleDto);
        verify(roleCrudService, times(1)).findByName(createRoleDto.getName());
        verify(roleCrudService, times(1)).save(any(Role.class));
        verify(permissionService, times(1)).findAllPermissionsByNames(createRoleDto.getPermissions());
        verify(roleLogService, times(1)).logCreatingEvent(expectedRole);
    }

    @Test
    public void createRoleWithSameNameError() {
        Role role = TestUtil.getRole();
        CreateRoleDto createRoleDto = TestUtil.getCreateRoleDto(role);
        when(roleCrudService.findByName(createRoleDto.getName()))
                .thenReturn(Optional.of(role));
        Assertions.assertThrows(RoleAlreadyExistException.class, () -> roleService.createRole(createRoleDto));
        verify(roleCrudService, times(1)).findByName(createRoleDto.getName());
    }

    @Test
    public void updateRoleOk() throws RoleNotFoundException, RoleCreationException {
        UpdateRoleDto updateRoleDto = TestUtil.getUpdateDto();
        Role role = TestUtil.getRole();
        Set<User> users = Set.of(TestUtil.getUser());
        role.setUsers(users);
        Long id = role.getId();
        Permission permission = TestUtil.getPermission();
        Set<Permission> permissions = new HashSet<>();
        permissions.add(permission);
        when(roleCrudService.findById(id)).thenReturn(role);
        when(permissionService.findAllPermissionsByNames(updateRoleDto.getPermissions()))
                .thenReturn(permissions);
        when(roleCrudService.saveAndFlush(any(Role.class))).thenReturn(role);
        when(serializationUtil.deepCopy(role, Role.class)).thenReturn(role);

        RoleDto updatedRole = roleService.updateRole(updateRoleDto, id);
        assertNotNull(updatedRole);
        verify(roleCrudService, times(1)).findById(id);
        verify(permissionService, times(1)).findAllPermissionsByNames(updateRoleDto.getPermissions());
        verify(roleCrudService, times(1)).saveAndFlush(any(Role.class));
        verify(roleLogService, times(1)).logUpdatingEvent(role, role);
    }

    @Test
    public void updateRoleNotFoundError() throws RoleNotFoundException {
        UpdateRoleDto updateRoleDto = TestUtil.getUpdateDto();
        Role role = TestUtil.getRole();
        Long id = role.getId();
        Permission permission = TestUtil.getPermission();
        List<Permission> permissions = new ArrayList<>();
        permissions.add(permission);
        when(roleCrudService.findById(id)).thenThrow(RoleNotFoundException.class);
        Assertions.assertThrows(RoleNotFoundException.class, () -> roleService.updateRole(updateRoleDto, id));
        verify(roleCrudService, times(1)).findById(id);
    }

    @Test
    public void deleteRole() throws RoleNotFoundException {
        Role role = TestUtil.getRole();
        Long id = role.getId();
        when(roleCrudService.findById(id)).thenReturn(role);
        roleService.deleteRole(id);
        verify(roleCrudService, times(1)).findById(id);
        verify(roleCrudService, times(1)).delete(role);
        verify(roleLogService, times(1)).logDeletingEvent(role);
    }

    @Test
    public void deleteRoleNotFound() throws RoleNotFoundException {
        Role role = TestUtil.getRole();
        Long id = role.getId();
        when(roleCrudService.findById(id)).thenThrow(RoleNotFoundException.class);
        Assertions.assertThrows(RoleNotFoundException.class, () -> roleService.deleteRole(id));
        verify(roleCrudService, times(1)).findById(id);
    }

    @Test
    void getRoleDtoOk() throws ApiException {
        Role role = TestUtil.getRole();
        Page<Role> pageOfRoles = TestUtil.getPageOfRoles(Collections.singletonList(role));
        PageRequestDto pageRequestDto = TestUtil.getPageRequestDto(SortDto.Order.DESC, "" +
                "id", "id", "1", FilterDto.Operator.EQ);
        when(roleCrudService.findByFilter(pageRequestDto))
                .thenReturn(pageOfRoles);
        PageResponseDto<RoleDto> userPageResponse = roleService.getRoleList(pageRequestDto);
        assertNotNull(userPageResponse);
        assertEquals(1, userPageResponse.getTotal().getCount());
        verify(roleCrudService, times(1))
                .findByFilter(pageRequestDto);
    }

    @Test
    public void deleteRoles() throws RoleNotFoundException {
        Role role = TestUtil.getRole();
        Long id = role.getId();
        when(roleCrudService.findById(id)).thenReturn(role);

        ListResponseDto<DeleteResponseDto> response = roleService.deleteRoles(List.of(role.getId()));
        assertNotNull(response);
        assertEquals(1, response.getResult().size());
        assertEquals(role.getId(), response.getResult().get(0).getId());
        assertNull(response.getResult().get(0).getError());

        verify(roleCrudService, times(1)).findById(id);
        verify(roleCrudService, times(1)).delete(role);
        verify(roleLogService, times(1)).logDeletingEvent(role);
    }

    @Test
    public void deleteRolesNotFound() throws RoleNotFoundException {
        Role role = TestUtil.getRole();
        Long id = role.getId();
        when(roleCrudService.findById(id)).thenThrow(new RoleNotFoundException(id));

        ListResponseDto<DeleteResponseDto> response = roleService.deleteRoles(List.of(role.getId()));
        assertNotNull(response);
        assertEquals(1, response.getResult().size());
        assertEquals(role.getId(), response.getResult().get(0).getId());
        assertEquals(Error.ROLE_NOT_FOUND.getCode(), response.getResult().get(0).getError().getCode());
        assertEquals(Error.ROLE_NOT_FOUND.getDescription(), response.getResult().get(0).getError().getDescription());

        verify(roleCrudService, times(1)).findById(id);
    }
}

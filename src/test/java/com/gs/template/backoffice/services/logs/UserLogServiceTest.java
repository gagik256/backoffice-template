package com.gs.template.backoffice.services.logs;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.log.UserLogDto;
import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import com.gs.template.backoffice.model.entities.elastic.log.UserLog;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.repositories.elastic.log.ElasticDao;
import com.gs.template.backoffice.repositories.elastic.log.UserLogElasticRepository;
import com.gs.template.backoffice.services.log.UserLogService;
import com.gs.template.backoffice.services.log.impl.UserLogServiceImpl;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;

import static com.gs.template.backoffice.services.log.impl.UserLogServiceImpl.USER_LOG_INDEX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = UserLogServiceImpl.class)
class UserLogServiceTest {

    @Autowired
    private UserLogService userLogService;

    @MockBean
    private UserLogElasticRepository userLogRepository;

    @MockBean
    private ElasticDao<UserLog> elasticDao;

    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(userLogRepository, elasticDao);
    }

    @Test
    public void logCreatingEventOk() {
        User user = TestUtil.getUser();

        userLogService.logCreatingEvent(user);

        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    public void logUpdatingEventOk() {
        User user = TestUtil.getUser();

        userLogService.logUpdatingEvent(user, user);

        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    public void logDeletingEventOk() {
        User user = TestUtil.getUser();

        userLogService.logDeletingEvent(user);

        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    public void getUsersLogs() {
        UserLog userLog = TestUtil.getUserLog(LogEventType.CREATE);
        List<UserLog> userLogs = Collections.singletonList(userLog);
        PageRequestDto pageRequestDto = TestUtil.getPageRequestDto(SortDto.Order.DESC, "" +
                "id", "id", "1", FilterDto.Operator.EQ);

        when(elasticDao.findByQuery(any(NativeSearchQuery.class), any(), eq(USER_LOG_INDEX))).thenReturn(userLogs);

        PageResponseDto<UserLogDto> usersLogsResponse = userLogService.getUsersLogs(pageRequestDto);

        assertNotNull(usersLogsResponse);
        assertEquals(1, usersLogsResponse.getTotal().getCount());

        verify(elasticDao, timeout(1)).findByQuery(any(NativeSearchQuery.class), any(), eq(USER_LOG_INDEX));
    }
}

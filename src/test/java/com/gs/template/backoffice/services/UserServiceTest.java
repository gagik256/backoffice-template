package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.user.CreateUserDto;
import com.gs.template.backoffice.dto.user.UpdateUserDto;
import com.gs.template.backoffice.dto.user.UserDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.UserAlreadyExistsException;
import com.gs.template.backoffice.errors.api.UserCreationException;
import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.services.impl.UserServiceImpl;
import com.gs.template.backoffice.services.log.UserLogService;
import com.gs.template.backoffice.util.TestUtil;
import com.gs.template.backoffice.utils.SerializationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = UserServiceImpl.class)
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @MockBean
    private UserCrudService userCrudService;
    @MockBean
    private RoleCrudService roleCrudService;
    @MockBean
    private UserLogService userLogService;
    @MockBean
    private SerializationUtil serializationUtil;
    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(userCrudService, roleCrudService);
    }

    @Test
    void createUserOk() throws UserAlreadyExistsException, UserCreationException {
        User user = TestUtil.getUser();
        CreateUserDto createUserDto = TestUtil.getCreateUserDto(user);
        when(userCrudService.findByLogin(createUserDto.getLogin()))
                .thenReturn(Optional.empty());
        when(userCrudService.findByEmail(createUserDto.getEmail()))
                .thenReturn(Optional.empty());
        when(roleCrudService.findByIds(createUserDto.getRoles()))
                .thenReturn(user.getRoles());
        when(userCrudService.save(any(User.class))).thenReturn(user);

        UserDto actualUser = userService.createUser(createUserDto);

        Assertions.assertEquals(UserDto.of(user), actualUser);
        verify(userCrudService, times(1)).findByLogin(createUserDto.getLogin());
        verify(userCrudService, times(1)).findByEmail(createUserDto.getEmail());
        verify(roleCrudService, times(1)).findByIds(createUserDto.getRoles());
        verify(userCrudService, times(1)).save(any(User.class));
        verify(userLogService, times(1)).logCreatingEvent(user);

    }

    @Test
    void createUserWithSameLoginError() {
        User user = TestUtil.getUser();
        CreateUserDto createUserDto = TestUtil.getCreateUserDto(user);
        createUserDto.setLogin("test");

        when(userCrudService.findByLogin(createUserDto.getLogin()))
                .thenReturn(Optional.of(user));

        assertThrows(UserAlreadyExistsException.class, () -> userService.createUser(createUserDto));

        verify(userCrudService, times(1)).findByLogin(createUserDto.getLogin());
    }

    @Test
    void createUserWithSameEmailError() {
        User user = TestUtil.getUser();
        CreateUserDto createUserDto = TestUtil.getCreateUserDto(user);
        createUserDto.setEmail("test");
        when(userCrudService.findByLogin(createUserDto.getLogin()))
                .thenReturn(Optional.empty());
        when(userCrudService.findByEmail(createUserDto.getEmail()))
                .thenReturn(Optional.of(user));

        assertThrows(UserAlreadyExistsException.class, () -> userService.createUser(createUserDto));

        verify(userCrudService, times(1)).findByLogin(createUserDto.getLogin());
        verify(userCrudService, times(1)).findByEmail(createUserDto.getEmail());
    }

    @Test
    void deleteUserOk() throws UserNotFoundException {
        User user = TestUtil.getUser();

        when(userCrudService.findById(user.getId())).thenReturn(user);

        userService.deleteUser(user.getId());

        verify(userCrudService, times(1)).findById(user.getId());
        verify(userCrudService, times(1)).delete(user);
        verify(userLogService, times(1)).logDeletingEvent(user);
    }

    @Test
    void deleteNotExistUser() throws UserNotFoundException {
        User user = TestUtil.getUser();

        when(userCrudService.findById(user.getId())).thenThrow(UserNotFoundException.class);

        assertThrows(UserNotFoundException.class, () -> userService.deleteUser(user.getId()));

        verify(userCrudService, times(1)).findById(user.getId());
    }

    @Test
    void updateUserOk() throws UserNotFoundException, UserAlreadyExistsException, UserCreationException {
        final String email = "test@gmail.com";
        User user = TestUtil.getUser();
        Set<Role> oldRoles = user.getRoles();
        Role role = TestUtil.getRole();
        UpdateUserDto updateUserDto = TestUtil.getUpdateUserDto(user);
        updateUserDto.setEmail(email);
        updateUserDto.setRoles(Set.of(role.getId()));

        when(userCrudService.findById(user.getId())).thenReturn(user);
        when(userCrudService.findByEmail(updateUserDto.getEmail()))
                .thenReturn(Optional.empty());
        when(roleCrudService.findByIds(updateUserDto.getRoles()))
                .thenReturn(Set.of(role));
        when(userCrudService.save(any(User.class))).thenReturn(user);
        when(serializationUtil.deepCopy(user, User.class)).thenReturn(user);

        UserDto updatedUser = userService.updateUser(user.getId(), updateUserDto);

        Assertions.assertEquals(UserDto.of(user), updatedUser);
        verify(userCrudService, times(1)).findById(user.getId());
        verify(userCrudService, times(1)).findByEmail(updateUserDto.getEmail());
        verify(roleCrudService, times(1)).findByIds(updateUserDto.getRoles());
        verify(userCrudService, times(1)).save(any(User.class));
        verify(userLogService, times(1)).logUpdatingEvent(user, user);
    }

    @Test
    void listUsersOk() throws ApiException {
        User user = TestUtil.getUser();
        Page<User> pageOfUsers = TestUtil.getPageOfUsers(Collections.singletonList(user));
        PageRequestDto pageRequestDto = TestUtil.getPageRequestDto(SortDto.Order.DESC, "" +
                "id", "id", "1", FilterDto.Operator.EQ);

        when(userCrudService.findByFilter(pageRequestDto))
                .thenReturn(pageOfUsers);

        PageResponseDto<UserDto> userPageResponse = userService.getUserList(pageRequestDto);

        assertNotNull(userPageResponse);
        assertEquals(1, userPageResponse.getTotal().getCount());

        verify(userCrudService, times(1)).findByFilter(pageRequestDto);
    }

    @Test
    void updateUserNotFoundError() throws UserNotFoundException {
        User user = TestUtil.getUser();
        UpdateUserDto updateUserDto = TestUtil.getUpdateUserDto(user);

        when(userCrudService.findById(user.getId())).thenThrow(UserNotFoundException.class);

        assertThrows(UserNotFoundException.class, () -> userService.updateUser(user.getId(), updateUserDto));

        verify(userCrudService, times(1)).findById(user.getId());
    }

    @Test
    void updateUserWithSameEmailError() throws UserNotFoundException {
        final String email = "test@gmail.com";
        User user = TestUtil.getUser();
        UpdateUserDto updateUserDto = TestUtil.getUpdateUserDto(user);
        updateUserDto.setEmail(email);

        when(userCrudService.findById(user.getId())).thenReturn(user);
        when(userCrudService.findByEmail(updateUserDto.getEmail()))
                .thenReturn(Optional.of(user));
        when(serializationUtil.deepCopy(user, User.class)).thenReturn(user);

        assertThrows(UserAlreadyExistsException.class, () -> userService.updateUser(user.getId(), updateUserDto));

        verify(userCrudService, times(1)).findById(user.getId());
        verify(userCrudService, times(1)).findByEmail(updateUserDto.getEmail());
    }


    @Test
    void deleteUsersOk() throws UserNotFoundException {
        User user = TestUtil.getUser();

        when(userCrudService.findById(user.getId())).thenReturn(user);

        ListResponseDto<DeleteResponseDto> response = userService.deleteUsers(List.of(user.getId()));
        assertNotNull(response);
        assertEquals(1, response.getResult().size());
        assertEquals(user.getId(), response.getResult().get(0).getId());
        assertNull(response.getResult().get(0).getError());

        verify(userCrudService, times(1)).findById(user.getId());
        verify(userCrudService, times(1)).delete(user);
    }
}

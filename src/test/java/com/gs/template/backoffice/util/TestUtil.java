package com.gs.template.backoffice.util;

import com.gs.template.backoffice.dto.*;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.role.CreateRoleDto;
import com.gs.template.backoffice.dto.role.UpdateRoleDto;
import com.gs.template.backoffice.dto.user.*;
import com.gs.template.backoffice.model.entities.elastic.log.*;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestUtil {
    private static final Random RANDOM = new Random();
    public static final int PAGE = 0;
    public static final int PAGE_SIZE = 20;

    public static CreateUserDto getCreateUserDto() {
        CreateUserDto createUserDto = new CreateUserDto();
        createUserDto.setLogin("Login" + RANDOM.nextInt());
        createUserDto.setPassword("Pass" + RANDOM.nextInt());
        createUserDto.setFirstName("FirstName" + RANDOM.nextInt());
        createUserDto.setLastName("LastName" + RANDOM.nextInt());
        createUserDto.setEmail("Email" + RANDOM.nextInt());
        createUserDto.setCountry("Country" + RANDOM.nextInt());
        createUserDto.setRoles(Set.of(RANDOM.nextLong()));
        return createUserDto;
    }

    public static Page<Role> getPageOfRoles(List<Role> roles) {
        return new PageImpl(roles);
    }

    public static PageRequestDto getPageRequestDto(SortDto.Order sortOrder,
                                                   String sortField,
                                                   String filterFieldName,
                                                   String filterFieldValue,
                                                   FilterDto.Operator filterOperator
    ) {
        PageRequestDto pageRequestDto = new PageRequestDto();
        pageRequestDto.setPage(PAGE);
        pageRequestDto.setPageSize(PAGE_SIZE);
        pageRequestDto.setSort(getSortDto(sortOrder, sortField));
        pageRequestDto.setFilters(getFilterDtos(filterFieldName, filterFieldValue, filterOperator));

        return pageRequestDto;
    }

    private static List<FilterDto> getFilterDtos(String filterFieldName,
                                                 String filterFieldValue,
                                                 FilterDto.Operator filterOperator) {
        FilterDto filterDto = new FilterDto(filterFieldName, filterOperator, filterFieldValue);
        return Collections.singletonList(filterDto);
    }

    private static SortDto getSortDto(SortDto.Order order, String sortField) {
        SortDto sortDto = new SortDto();
        sortDto.setOrder(order);
        sortDto.setField(sortField);
        return sortDto;
    }

    public static UserDto getUserDto() {
        return new UserDto(
                RANDOM.nextLong(),
                "login" + RANDOM.nextInt(),
                "email" + RANDOM.nextInt(),
                "firstName" + RANDOM.nextInt(),
                "lastName" + RANDOM.nextInt(),
                "country" + RANDOM.nextInt(),
                RANDOM.nextLong(),
                RANDOM.nextLong(),
                Set.of("Role" + RANDOM.nextInt()));
    }

    public static User getUser() {
        User user = new User("login" + RANDOM.nextInt(),
                "password" + RANDOM.nextInt(),
                "firstName" + RANDOM.nextInt(),
                "lastName" + RANDOM.nextInt(),
                "email" + RANDOM.nextInt(),
                "country" + RANDOM.nextInt(),
                Set.of(getRole()));
        user.setId(RANDOM.nextLong());
        return user;
    }

    public static Set<User> getUsers() {
        Set<User> users = new HashSet<>();
        User user = new User("login" + RANDOM.nextInt(),
                "pass" + RANDOM.nextInt(),
                "name" + RANDOM.nextInt(),
                "lname" + RANDOM.nextInt(),
                "email" + RANDOM.nextInt(),
                "country" + RANDOM.nextInt(),
                new HashSet<>());
        user.setId(1L);
        users.add(user);
        return users;
    }

    public static Page<User> getPageOfUsers(List<User> users) {
        return new PageImpl(users);
    }

    public static CreateUserDto getCreateUserDto(User user) {
        CreateUserDto createUserDto = new CreateUserDto();
        createUserDto.setLogin(user.getLogin());
        createUserDto.setPassword(user.getPassword());
        createUserDto.setFirstName(user.getFirstName());
        createUserDto.setLastName(user.getLastName());
        createUserDto.setEmail(user.getEmail());
        createUserDto.setCountry(user.getCountry());
        createUserDto.setRoles(user.getRoles().stream().map(Role::getId).collect(Collectors.toSet()));
        return createUserDto;
    }

    public static UpdateUserDto getUpdateUserDto(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();
        updateUserDto.setFirstName(user.getFirstName());
        updateUserDto.setLastName(user.getLastName());
        updateUserDto.setEmail(user.getEmail());
        updateUserDto.setCountry(user.getCountry());
        updateUserDto.setRoles(user.getRoles().stream().map(Role::getId).collect(Collectors.toSet()));
        return updateUserDto;
    }

    public static Role getRole() {
        Role role = new Role("name" + RANDOM.nextInt(),
                "description" + RANDOM.nextInt(),
                getPermissions(),
                getUsers());
        role.setId(RANDOM.nextLong());
        return role;
    }

    public static Long getLong() {
        return RANDOM.nextLong();
    }

    public static Set<Long> getUsersId() {
        Set<Long> ids = new HashSet<>();
        ids.add((long) 1);
        return ids;
    }

    public static String getString() {
        return "SomeString" + RANDOM.nextInt();
    }

    public static Permission getPermission() {
        Permission permission = new Permission("name" + RANDOM.nextInt(), "description" + RANDOM.nextInt());
        permission.setId(RANDOM.nextLong());
        return permission;
    }

    public static Set<Permission> getPermissions() {
        Set<Permission> permissions = new HashSet<>();
        permissions.add(getPermission());
        return permissions;
    }

    public static CreateRoleDto getCreateRoleDto() {
        CreateRoleDto createRoleDto = new CreateRoleDto();
        createRoleDto.setName("Name" + RANDOM.nextInt());
        createRoleDto.setPermissions(Set.of(getPermission().toString()));
        createRoleDto.setDescription("Description" + RANDOM.nextInt());

        return createRoleDto;
    }

    public static CreateRoleDto getCreateRoleDto(Role role) {
        CreateRoleDto createRoleDto = new CreateRoleDto();
        createRoleDto.setName(role.getName());
        createRoleDto.setDescription(role.getDescription());
        createRoleDto.setPermissions(role.getPermissions().stream().map(Permission::getName).collect(toSet()));
        return createRoleDto;
    }

    public static UpdateRoleDto getUpdateDto() {
        UpdateRoleDto updateRoleDto = new UpdateRoleDto();
        updateRoleDto.setDescription("Description" + RANDOM.nextInt());
        updateRoleDto.setPermissions(Set.of(getPermission().toString()));
        return updateRoleDto;
    }

    public static UserLog getUserLog(LogEventType eventType) {
        return UserLog.builder()
                .id("id" + RANDOM.nextLong())
                .eventType(eventType)
                .userChangedFields(UserChangedFields.builder()
                        .loginOldValue("loginOldValue" + RANDOM.nextInt())
                        .loginNewValue("loginNewValue" + RANDOM.nextInt())
                        .firstNameOldValue("firstNameOldValue" + RANDOM.nextInt())
                        .firstNameNewValue("firstNameNewValue" + RANDOM.nextInt())
                        .lastNameOldValue("lastNameOldValue" + RANDOM.nextInt())
                        .lastNameNewValue("lastNameNewValue" + RANDOM.nextInt())
                        .countryOldValue("countryOldValue" + RANDOM.nextInt())
                        .countryNewValue("countryNewValue" + RANDOM.nextInt())
                        .emailOldValue("emailOldValue" + RANDOM.nextInt())
                        .emailNewValue("emailNewValue" + RANDOM.nextInt())
                        .rolesOldValue("rolesOldValue" + RANDOM.nextInt())
                        .rolesNewValue("rolesNewValue" + RANDOM.nextInt())
                        .build())
                .createdAt(RANDOM.nextLong())
                .build();
    }

    public static Page<UserLog> getPageOfUserLogs(List<UserLog> userLogs) {
        return new PageImpl(userLogs);
    }


    public static RoleLog getRoleLog(LogEventType eventType) {
        Set<PermissionChange> permissionChanges = new HashSet<>();
        Set<UserChange> userChanges = new HashSet<>();
        permissionChanges.add(PermissionChange.builder()
                .changedType(ChangedType.ADDED)
                .permission(PermissionDto.of(getPermission()))
                .build());
        userChanges.add(UserChange.builder()
                .changedType(ChangedType.ADDED)
                .user(ShortUserDto.of(getUser()))
                .build());
        return RoleLog.builder()
                .id("id" + RANDOM.nextLong())
                .eventType(eventType)
                .roleChangedFields(RoleChangedFields.builder()
                        .id(RANDOM.nextLong())
                        .descriptionNewValue("descriptionNewValue" + RANDOM.nextLong())
                        .descriptionOldValue("descriptionOldValue" + RANDOM.nextLong())
                        .nameNewValue("nameNewValue" + RANDOM.nextLong())
                        .nameOldValue("nameOldValue" + RANDOM.nextLong())
                        .permissionChanges(permissionChanges)
                        .userChanges(userChanges)
                        .build())
                .createdAt(RANDOM.nextLong())
                .build();
    }

    public static Page<RoleLog> getPageOfRoleLogs(List<RoleLog> roleLogs) {
        return new PageImpl(roleLogs);
    }
}

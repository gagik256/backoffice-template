package com.gs.template.backoffice.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = SerializationUtil.class)
class SerializationUtilTest {

    @Autowired
    private SerializationUtil serializationUtil;
    @MockBean
    private ObjectMapper objectMapper;

    @Test
    public void deepCopyTest() throws JsonProcessingException {
        User user = TestUtil.getUser();

        when(objectMapper.writeValueAsString(user)).thenReturn("");
        when(objectMapper.readValue("", User.class)).thenReturn(user);
        User userCopy = serializationUtil.deepCopy(user, User.class);

        assertEquals(user, userCopy);
        verify(objectMapper, times(1)).writeValueAsString(user);
        verify(objectMapper, times(1)).readValue("", User.class);
    }
}

# Naming policy for a changelog
File `db-migrations/changelog.xml` imports files `/db-migrations/{version}/changelog.xml`
Files `/db-migrations/{version}/changelog.xml` import files with changesets in current folders

## Naming policy for folders of changelogs
 - Folders for each version of the application are in the folder `db-migrations`
 - Folders for a specific version are being named `v{version}` 
 
   Example: v0.1
 
## Naming policy for files with a changeset
 - Files are being named `{yyyy-MM-dd}_{serial number of the file in the folder}-{short description}.xml`. 

   Example: 2020-07-02_0-initial-schema.xml 

## Naming policy for a changeset's id
 - Changeset's IDs are being named `{Application version}.{Serial number of the file}.{Serial number of the changeset in the file}` 
    
   Example for the second changeset in a file v0.1/2020-07-02_0-indices.xml: id="0.1.2.2"
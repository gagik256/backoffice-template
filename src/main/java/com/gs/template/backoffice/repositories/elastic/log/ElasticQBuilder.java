package com.gs.template.backoffice.repositories.elastic.log;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.errors.services.IllegalEnumNameException;
import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class ElasticQBuilder {

    private static final Map<String, ElasticQBuilder.FieldSpec<?>> USER_LOG_FILTERS = buildUserLogFilters();
    private static final Map<String, List<ElasticQBuilder.FieldSpec<?>>> USER_LOG_FILTERS_LIST = buildUserLogFiltersList();
    private static final Map<String, ElasticQBuilder.FieldSpec<?>> ROLE_LOG_FILTERS = buildRoleLogFilters();
    private static final Map<String, List<ElasticQBuilder.FieldSpec<?>>> ROLE_LOG_FILTERS_LIST = buildRoleLogFiltersList();

    public static QueryBuilder toUserLogQuery(PageRequestDto pageRequestDto) {
        QueryBuilder query = buildQuery(pageRequestDto, USER_LOG_FILTERS);
        return buildQuery(query, pageRequestDto, USER_LOG_FILTERS_LIST);
    }

    public static QueryBuilder toRoleLogQuery(PageRequestDto pageRequestDto) {
        QueryBuilder query = buildQuery(pageRequestDto, ROLE_LOG_FILTERS);
        return buildQuery(query, pageRequestDto, ROLE_LOG_FILTERS_LIST);
    }

    public static QueryBuilder buildQuery(PageRequestDto pageRequestDto, Map<String, ElasticQBuilder.FieldSpec<?>> filterItem) {
        List<FilterDto> filters = pageRequestDto.getFilters();
        if (filters == null || filters.isEmpty()) {
            return matchAll();
        }
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        for (FilterDto filter : filters) {
            ElasticQBuilder.FieldSpec<?> fieldSpec = filterItem.get(filter.getField());
            if (fieldSpec == null) {
                log.error("Invalid filter field: {}", filter.getField());
                continue;
            }
            QueryBuilder queryBuilder = getQuery(fieldSpec, filter.getOperator(), filter.getValue());
            if (queryBuilder != null) {
                boolQuery.must(queryBuilder);
            }
        }
        return boolQuery;
    }

    public static QueryBuilder buildQuery(QueryBuilder query, PageRequestDto pageRequestDto, Map<String, List<ElasticQBuilder.FieldSpec<?>>> filterItems) {
        List<FilterDto> filters = pageRequestDto.getFilters();
        if (filters == null || filters.isEmpty()) {
            return matchAll();
        }
        for (FilterDto filter : filters) {
            List<ElasticQBuilder.FieldSpec<?>> fieldSpecList = filterItems.get(filter.getField());
            if (CollectionUtils.isEmpty(fieldSpecList)) {
                log.error("Invalid filter field: {}", filter.getField());
                continue;
            }
            BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
            for (ElasticQBuilder.FieldSpec<?> fieldSpec : fieldSpecList) {
                QueryBuilder queryBuilder = getQuery(fieldSpec, filter.getOperator(), filter.getValue());
                if (queryBuilder != null) {
                    boolQuery.should(queryBuilder);
                }
            }
            BoolQueryBuilder boolQueryBuilder = (BoolQueryBuilder) query;
            boolQueryBuilder.must(boolQuery);
        }
        return query;
    }

    private static <T> QueryBuilder getQuery(ElasticQBuilder.FieldSpec<T> fieldSpec, FilterDto.Operator operator, String value) {
        String fieldName = fieldSpec.getFieldName();
        Function<String, T> converter = fieldSpec.getConverter();
        switch (operator) {
            case EQ:
                return QueryBuilders.termQuery(fieldName, converter.apply(value));
            case GE:
                return QueryBuilders.rangeQuery(fieldName).gte(converter.apply(value));
            case LE:
                return QueryBuilders.rangeQuery(fieldName).lte(converter.apply(value));
            default:
                throw new IllegalStateException("Unknown operator: " + operator);
        }
    }

    private static Map<String, ElasticQBuilder.FieldSpec<?>> buildUserLogFilters() {
        //@formatter:off
        return Map.ofEntries(
                Map.entry("eventType", ofEnum("eventType.keyword", LogEventType.class)),
                Map.entry("createdAt", ofLong("createdAt"))
        );
        //@formatter:on
    }

    private static Map<String, List<ElasticQBuilder.FieldSpec<?>>> buildUserLogFiltersList() {
        //@formatter:off
        return Map.ofEntries(
                Map.entry("login", List.of(
                        ofString("userChangedFields.loginOldValue.keyword"),
                        ofString("userChangedFields.loginNewValue.keyword"))),
                Map.entry("email", List.of(
                        ofString("userChangedFields.emailOldValue.keyword"),
                        ofString("userChangedFields.emailNewValue.keyword"))),
                Map.entry("firstName", List.of(
                        ofString("userChangedFields.firstNameOldValue.keyword"),
                        ofString("userChangedFields.firstNameNewValue.keyword"))),
                Map.entry("lastName", List.of(
                        ofString("userChangedFields.lastNameOldValue.keyword"),
                        ofString("userChangedFields.lastNameNewValue.keyword"))),
                Map.entry("country", List.of(
                        ofString("userChangedFields.countryOldValue.keyword"),
                        ofString("userChangedFields.countryNewValue.keyword")))
        );
        //@formatter:on
    }

    private static Map<String, ElasticQBuilder.FieldSpec<?>> buildRoleLogFilters() {
        //@formatter:off
        return Map.ofEntries(
                Map.entry("eventType", ofEnum("eventType.keyword", LogEventType.class)),
                Map.entry("createdAt", ofLong("createdAt"))
        );
        //@formatter:on
    }

    private static Map<String, List<ElasticQBuilder.FieldSpec<?>>> buildRoleLogFiltersList() {
        //@formatter:off
        return Map.ofEntries(
                Map.entry("name", List.of(
                        ofString("roleLog.roleChangedFields.nameOldValue.keyword"),
                        ofString("roleLog.roleChangedFields.nameNewValue.keyword"))),
                Map.entry("description", List.of(
                        ofString("roleLog.roleChangedFields.descriptionOldValue.keyword"),
                        ofString("roleLog.roleChangedFields.descriptionNewValue.keyword")))
        );
        //@formatter:on
    }

    private static ElasticQBuilder.FieldSpec<Long> ofLong(String fieldName) {
        return new ElasticQBuilder.FieldSpec<>(fieldName, Long::valueOf);
    }

    private static <T extends Enum<T>> ElasticQBuilder.FieldSpec<T> ofEnum(String fieldName, Class<T> type) {
        return new ElasticQBuilder.FieldSpec<>(fieldName, s -> getEnumValue(type, s.toUpperCase()));
    }

    private static ElasticQBuilder.FieldSpec<BigDecimal> ofBigDecimal(String fieldName) {
        return new ElasticQBuilder.FieldSpec<>(fieldName, BigDecimal::new);
    }

    private static ElasticQBuilder.FieldSpec<String> ofString(String fieldName) {
        return new ElasticQBuilder.FieldSpec<>(fieldName, Function.identity());
    }

    private static ElasticQBuilder.FieldSpec<Boolean> ofBoolean(String fieldName) {
        return new ElasticQBuilder.FieldSpec<>(fieldName, Boolean::parseBoolean);
    }

    private static <T extends Enum<T>> T getEnumValue(Class<T> type, String value) {
        try {
            return Enum.valueOf(type, value);
        } catch (IllegalArgumentException e) {
            throw new IllegalEnumNameException(type, value, e);
        }
    }

    private static QueryBuilder matchAll() {
        return QueryBuilders.matchAllQuery();
    }

    @Value
    private static class FieldSpec<T> {
        String fieldName;
        Function<String, T> converter;
    }
}

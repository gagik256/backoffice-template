package com.gs.template.backoffice.repositories.jpa;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.*;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.errors.services.IllegalEnumNameException;
import com.gs.template.backoffice.model.entities.jpa.QRole;
import com.gs.template.backoffice.model.entities.jpa.QUser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public final class QPredicates {

    private static final Map<String, FieldSpec<?>> USER_EXPRESSIONS = buildUserExpressions();
    private static final Map<String, FieldSpec<?>> ROLE_EXPRESSIONS = buildRoleExpressions();
    private static final Map<String, FieldSpec<?>> USER_LOG_EXPRESSIONS = buildUserLogExpressions();
    private static final Map<String, List<FieldSpec<?>>> USER_LOG_EXPRESSIONS_LIST = buildUserLogExpressionsList();
    private static final Map<String, FieldSpec<?>> ROLE_LOG_EXPRESSIONS = buildRoleLogExpressions();
    private static final Map<String, List<FieldSpec<?>>> ROLE_LOG_EXPRESSIONS_LIST = buildRoleLogExpressionsList();


    public static BooleanExpression toUserPredicate(List<FilterDto> filters) {
        return toPredicate(filters, USER_EXPRESSIONS);
    }
    public static BooleanExpression toRolePredicate(List<FilterDto> filters) {
        return toPredicate(filters, ROLE_EXPRESSIONS);
    }
    public static BooleanExpression toUserLogPredicate(List<FilterDto> filters) {
        BooleanExpression booleanExpression = toPredicate(filters, USER_LOG_EXPRESSIONS);
        return toPredicate(booleanExpression, filters, USER_LOG_EXPRESSIONS_LIST);
    }
    public static BooleanExpression toRoleLogPredicate(List<FilterDto> filters) {
        BooleanExpression booleanExpression = toPredicate(filters, ROLE_LOG_EXPRESSIONS);
        return toPredicate(booleanExpression, filters, ROLE_LOG_EXPRESSIONS_LIST);
    }

    /**
     * This method for supporting SQL AND operation
     * @param filters
     * @param expressions
     * @return
     */
    private static BooleanExpression toPredicate(List<FilterDto> filters, Map<String, FieldSpec<?>> expressions) {
        if (filters == null || filters.isEmpty()) {
            return trueExpr();
        }
        BooleanExpression predicate = trueExpr();
        for (FilterDto filter : filters) {
            FieldSpec<?> fieldSpec = expressions.get(filter.getField());
            if (fieldSpec == null) {
                log.error("Invalid filter field: {}", filter.getField());
                continue;
            }
            Predicate pred = getPredicate(fieldSpec, filter.getOperator(), filter.getValue());
            if (pred != null) {
                predicate = predicate.and(pred);
            }
        }
        return predicate;
    }

    /**
     * This method for supporting SQL OR operation
     * @param predicate
     * @param filters
     * @param expressions
     * @return
     */
    private static BooleanExpression toPredicate(BooleanExpression predicate, List<FilterDto> filters, Map<String,  List<FieldSpec<?>>> expressions) {
        if (filters == null || filters.isEmpty()) {
            return predicate;
        }
        for (FilterDto filter : filters) {
            BooleanExpression orPredicate = falseExpr();
            List<FieldSpec<?>> fieldSpecList = expressions.get(filter.getField());
            if (CollectionUtils.isEmpty(fieldSpecList)) {
                log.error("Invalid filter field: {}", filter.getField());
                continue;
            }
            for (FieldSpec<?> fieldSpec: fieldSpecList) {
                Predicate pred = getPredicate(fieldSpec, filter.getOperator(), filter.getValue());
                if (pred != null) {
                    orPredicate = orPredicate.or(pred);
                }
            }
            predicate = predicate.and(orPredicate);
        }
        return predicate;
    }

    @Nullable
    @SuppressWarnings("unchecked")
    private static <T> Predicate getPredicate(FieldSpec<T> fieldSpec, FilterDto.Operator operator, String value) {
        SimpleExpression<T> expr = fieldSpec.getExpression();
        Function<String, T> converter = fieldSpec.getConverter();
        switch (operator) {
            case EQ: {
                return expr.eq(converter.apply(value));
            }
            case GE: {
                if (expr instanceof NumberExpression) {
                    return ((NumberExpression) expr).goe((Number) converter.apply(value));
                } else if (expr instanceof ComparableExpression) {
                    return ((ComparableExpression) expr).goe((Comparable) converter.apply(value));
                }
                log.warn("{} is not comparable", fieldSpec.getExpression());
                return null;
            }
            case LE: {
                if (expr instanceof NumberExpression) {
                    return ((NumberExpression) expr).loe((Number) converter.apply(value));
                } else if (expr instanceof ComparableExpression) {
                    return ((ComparableExpression) expr).loe((Comparable) converter.apply(value));
                }
                log.warn("{} is not comparable", fieldSpec.getExpression());
                return null;
            }
            default:
                throw new IllegalStateException("Unknown operator: " + operator);
        }
    }

    private static BooleanExpression trueExpr() {
        return Expressions.asNumber(1).eq(1);
    }

    private static BooleanExpression falseExpr() {
        return Expressions.asNumber(1).eq(2);
    }

    private static Map<String, FieldSpec<?>> buildUserExpressions() {
        //@formatter:off
        return Map.ofEntries(
                Map.entry("id", ofLong(QUser.user.id)),
                Map.entry("login", ofString(QUser.user.login)),
                Map.entry("firstName", ofString(QUser.user.firstName)),
                Map.entry("lastName", ofString(QUser.user.lastName)),
                Map.entry("email", ofString(QUser.user.email)),
                Map.entry("country", ofString(QUser.user.country)),
                Map.entry("createdAt", ofLong(QUser.user.createdAt)),
                Map.entry("updatedAt", ofLong(QUser.user.updatedAt))
        );
        //@formatter:on
    }

    private static Map<String, FieldSpec<?>> buildRoleExpressions() {
        return Map.ofEntries(
                Map.entry("id", ofLong(QRole.role.id)),
                Map.entry("name", ofString(QRole.role.name)),
                Map.entry("updatedAt", ofLong(QRole.role.updatedAt)),
                Map.entry("createdAt", ofLong(QRole.role.createdAt))
        );
    }

    private static Map<String, FieldSpec<?>> buildUserLogExpressions() {
        //@formatter:off
        return Map.ofEntries(
                /*Map.entry("eventType", ofEnum(QUserLog.userLog.eventType, LogEventType.class)),
                Map.entry("createdAt", ofLong(QUserLog.userLog.createdAt))*/
        );
        //@formatter:on
    }

    private static Map<String, List<FieldSpec<?>>> buildUserLogExpressionsList() {
        //@formatter:off
        return Map.ofEntries(
                /*Map.entry("login", List.of(
                        ofString(QUserLog.userLog.userChangedFields.loginOldValue),
                        ofString(QUserLog.userLog.userChangedFields.loginNewValue))),
                Map.entry("email", List.of(
                        ofString(QUserLog.userLog.userChangedFields.emailOldValue),
                        ofString(QUserLog.userLog.userChangedFields.emailNewValue))),
                Map.entry("firstName", List.of(
                        ofString(QUserLog.userLog.userChangedFields.firstNameOldValue),
                        ofString(QUserLog.userLog.userChangedFields.firstNameNewValue))),
                Map.entry("lastName", List.of(
                        ofString(QUserLog.userLog.userChangedFields.lastNameOldValue),
                        ofString(QUserLog.userLog.userChangedFields.lastNameNewValue))),
                Map.entry("country", List.of(
                        ofString(QUserLog.userLog.userChangedFields.countryOldValue),
                        ofString(QUserLog.userLog.userChangedFields.countryNewValue)))*/
        );
        //@formatter:on
    }

    private static Map<String, FieldSpec<?>> buildRoleLogExpressions() {
        //@formatter:off
        return Map.ofEntries(
                /*Map.entry("eventType", ofEnum(QRoleLog.roleLog.eventType, LogEventType.class)),
                Map.entry("createdAt", ofLong(QRoleLog.roleLog.createdAt))*/
        );
        //@formatter:on
    }

    private static Map<String, List<FieldSpec<?>>> buildRoleLogExpressionsList() {
        //@formatter:off
        return Map.ofEntries(
                /*Map.entry("name", List.of(
                        ofString(QRoleLog.roleLog.roleChangedFields.nameOldValue),
                        ofString(QRoleLog.roleLog.roleChangedFields.nameNewValue))),
                Map.entry("description", List.of(
                        ofString(QRoleLog.roleLog.roleChangedFields.descriptionOldValue),
                        ofString(QRoleLog.roleLog.roleChangedFields.descriptionNewValue)))*/
        );
        //@formatter:on
    }

    private static FieldSpec<Long> ofLong(NumberPath<Long> expression) {
        return new FieldSpec<>(expression, Long::valueOf);
    }

    private static <T extends Enum<T>> FieldSpec<T> ofEnum(EnumPath<T> expression, Class<T> type) {
        return new FieldSpec<>(expression, s -> getEnumValue(type, s.toUpperCase()));
    }

    private static FieldSpec<BigDecimal> ofBigDecimal(NumberPath<BigDecimal> expression) {
        return new FieldSpec<>(expression, BigDecimal::new);
    }

    private static FieldSpec<String> ofString(StringPath expression) {
        return new FieldSpec<>(expression, Function.identity());
    }

    private static FieldSpec<Boolean> ofBoolean(BooleanPath expression) {
        return new FieldSpec<>(expression, Boolean::parseBoolean);
    }

    private static <T extends Enum<T>> T getEnumValue(Class<T> type, String value) {
        try {
            return Enum.valueOf(type, value);
        } catch (IllegalArgumentException e) {
            throw new IllegalEnumNameException(type, value, e);
        }
    }

    @Value
    private static class FieldSpec<T> {
        SimpleExpression<T> expression;
        Function<String, T> converter;
    }
}

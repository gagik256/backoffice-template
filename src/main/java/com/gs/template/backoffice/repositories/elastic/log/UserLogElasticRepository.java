package com.gs.template.backoffice.repositories.elastic.log;

import com.gs.template.backoffice.model.entities.elastic.log.UserLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLogElasticRepository extends ElasticsearchRepository<UserLog, Long> {
}

package com.gs.template.backoffice.model.entities.elastic.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;


@Builder
@Data
@Document(indexName = "role_log")
@AllArgsConstructor
@NoArgsConstructor
public class RoleLog {
    private String id;
    private LogEventType eventType;
    private RoleChangedFields roleChangedFields;
    private Long createdAt;
}

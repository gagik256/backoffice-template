package com.gs.template.backoffice.model.entities.elastic.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Set;

/**
 * Backoffice role change fields
 */
@Builder
@Data
@Document(indexName = "role_changed_fields")
@AllArgsConstructor
@NoArgsConstructor
public class RoleChangedFields {
    @Id
    private Long id;
    private String nameOldValue;
    private String nameNewValue;
    private String descriptionOldValue;
    private String descriptionNewValue;
    private Set<UserChange> userChanges;
    private Set<PermissionChange> permissionChanges;
}

package com.gs.template.backoffice.model.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {
    private final int code;
    private final String description;

    public static ErrorResponse of(Error error) {
        return new ErrorResponse(error.getCode(), error.getDescription());
    }
}

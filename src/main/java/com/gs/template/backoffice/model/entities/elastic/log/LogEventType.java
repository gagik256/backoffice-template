package com.gs.template.backoffice.model.entities.elastic.log;

public enum LogEventType {
    CREATE,
    DELETE,
    UPDATE
}

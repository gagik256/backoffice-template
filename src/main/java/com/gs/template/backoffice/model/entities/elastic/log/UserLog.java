package com.gs.template.backoffice.model.entities.elastic.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Backoffice user log
 */
@Builder
@Data
@Document(indexName = "user_log")
@AllArgsConstructor
@NoArgsConstructor
public class UserLog {
    private String id;
    private LogEventType eventType;
    private UserChangedFields userChangedFields;
    private Long createdAt;
}

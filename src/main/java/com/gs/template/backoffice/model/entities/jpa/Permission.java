package com.gs.template.backoffice.model.entities.jpa;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * Backoffice permission
 */
@Data
@Entity
@Table(name = "permissions")
@NoArgsConstructor
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String description;

    public Permission(String name, String description) {
        this.name = name;
        this.description = description;
    }
}

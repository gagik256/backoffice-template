package com.gs.template.backoffice.model.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Error {
    NOT_FOUND(1, "Resource not found"),
    FORBIDDEN(2, "Operation not allowed"),
    BAD_REQUEST(3, "Invalid request format or parameters values"),
    USER_NOT_FOUND(4, "User not found"),
    LOGIN_ALREADY_EXISTS(5, "Login already exists"),
    EMAIL_ALREADY_EXISTS(6, "Email already exists"),

    ROLE_NAME_ALREADY_EXISTS(7,"Role name already exist"),
    ROLE_NOT_FOUND(8, "Role not found"),
    SERVER_ERROR(9, "Server error"),
    PERMISSION_NOT_FOUND(10, "Permission not found"),
    DEPENDENCIES_ERROR(11, "The entity has dependencies")
    ;

    private final int code;
    private final String description;
}

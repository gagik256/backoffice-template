package com.gs.template.backoffice.model.entities.elastic.log;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Backoffice user change fields
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserChangedFields {
    private String loginOldValue;
    private String loginNewValue;
    private String firstNameOldValue;
    private String firstNameNewValue;
    private String lastNameOldValue;
    private String lastNameNewValue;
    private String countryOldValue;
    private String countryNewValue;
    private String emailOldValue;
    private String emailNewValue;
    private String rolesOldValue;
    private String rolesNewValue;
}

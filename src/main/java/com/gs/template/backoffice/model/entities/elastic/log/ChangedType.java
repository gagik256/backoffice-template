package com.gs.template.backoffice.model.entities.elastic.log;

public enum ChangedType {
    ADDED,
    DELETED,
    IMMUTABLE
}

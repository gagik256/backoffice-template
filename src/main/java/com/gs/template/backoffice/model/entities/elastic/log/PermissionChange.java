package com.gs.template.backoffice.model.entities.elastic.log;

import com.gs.template.backoffice.dto.PermissionDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PermissionChange {
    private ChangedType changedType;
    private PermissionDto permission;
}

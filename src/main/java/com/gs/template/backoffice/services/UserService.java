package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.user.CreateUserDto;
import com.gs.template.backoffice.dto.user.UpdateUserDto;
import com.gs.template.backoffice.dto.user.UserDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.UserAlreadyExistsException;
import com.gs.template.backoffice.errors.api.UserCreationException;
import com.gs.template.backoffice.errors.api.UserNotFoundException;

import java.util.List;

public interface UserService {

    /**
     * Create user from dto
     *
     * @param userDto - dto for create
     * @return dto from created user
     * @throws UserAlreadyExistsException - if user with same login or email exists
     * @throws UserCreationException      - if roles not found
     */
    UserDto createUser(CreateUserDto userDto)
            throws UserAlreadyExistsException, UserCreationException;

    /**
     * Update user from dto
     *
     * @param id        id of the user
     * @param updateDto dto for updating the user
     * @return Updated user
     */
    UserDto updateUser(Long id, UpdateUserDto updateDto)
            throws UserNotFoundException, UserAlreadyExistsException, UserCreationException;

    /**
     * Delete user by id
     *
     * @param id id of the user
     */
    void deleteUser(Long id) throws UserNotFoundException;

    /**
     * Delete users by ids
     *
     * @param ids
     * @return
     */
    ListResponseDto<DeleteResponseDto> deleteUsers(List<Long> ids);


    /**
     * Find all users that matches specified filters
     *
     * @param pageRequestDto request user that contains paging, filtering and sorting
     * @return users that matches specified filters
     */
    PageResponseDto<UserDto> getUserList(PageRequestDto pageRequestDto) throws ApiException;
}

package com.gs.template.backoffice.services.impl;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.user.CreateUserDto;
import com.gs.template.backoffice.dto.user.UpdateUserDto;
import com.gs.template.backoffice.dto.user.UserDto;
import com.gs.template.backoffice.errors.api.UserAlreadyExistsException;
import com.gs.template.backoffice.errors.api.UserCreationException;
import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.model.api.Error;
import com.gs.template.backoffice.model.api.ErrorResponse;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.services.RoleCrudService;
import com.gs.template.backoffice.services.UserCrudService;
import com.gs.template.backoffice.services.UserService;
import com.gs.template.backoffice.services.log.UserLogService;
import com.gs.template.backoffice.utils.SerializationUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserCrudService userCrudService;
    private final RoleCrudService roleCrudService;
    private final UserLogService userLogService;
    private final SerializationUtil serializationUtil;

    @Override
    @Transactional
    public UserDto createUser(CreateUserDto userDto)
            throws UserAlreadyExistsException, UserCreationException {
        checkLoginExist(userDto.getLogin());
        checkEmailExist(userDto.getEmail());

        Set<Role> roles = getRolesForCreateOrUpdateUser(userDto.getRoles());

        User savedUser = userCrudService.save(new User(
                userDto.getLogin().trim(),
                userDto.getPassword().trim(),
                StringUtils.isNotBlank(userDto.getFirstName()) ? userDto.getFirstName().trim() : StringUtils.EMPTY,
                StringUtils.isNotBlank(userDto.getLastName()) ? userDto.getLastName().trim() : StringUtils.EMPTY,
                userDto.getEmail().trim().toLowerCase(),
                StringUtils.isNotBlank(userDto.getCountry()) ? userDto.getCountry().trim() : StringUtils.EMPTY,
                roles));

        userLogService.logCreatingEvent(savedUser);
        return UserDto.of(savedUser);
    }

    @Override
    @Transactional
    public UserDto updateUser(Long id, UpdateUserDto dto)
            throws UserNotFoundException, UserAlreadyExistsException, UserCreationException {
        User user = userCrudService.findById(id);
        User oldUser = deepCopy(user);
        boolean updated = false;
        if (StringUtils.isNotBlank(dto.getFirstName())
                && !dto.getFirstName().trim().equals(user.getFirstName())) {
            user.setFirstName(dto.getFirstName().trim());
            updated = true;
        }
        if (StringUtils.isNotBlank(dto.getLastName())
                && !dto.getLastName().trim().equals(user.getLastName())) {
            user.setLastName(dto.getLastName().trim());
            updated = true;
        }
        if (StringUtils.isNotBlank(dto.getEmail())
                && !dto.getEmail().trim().equalsIgnoreCase(user.getEmail())) {
            checkEmailExist(dto.getEmail());
            user.setEmail(dto.getEmail().trim().toLowerCase());
            updated = true;
        }
        if (StringUtils.isNotBlank(dto.getCountry())
                && !dto.getCountry().trim().equals(user.getCountry())) {
            user.setCountry(dto.getCountry().trim());
            updated = true;
        }
        if (dto.getRoles() != null) {
            if (dto.getRoles().isEmpty()) {
                if (!user.getRoles().isEmpty()) {
                    user.setRoles(Collections.emptySet());
                    updated = true;
                }
            } else {
                Set<Long> userRoles = user.getRoles().stream().map(Role::getId).collect(Collectors.toSet());
                if (!userRoles.equals(dto.getRoles())) {
                    Set<Role> newRoles = getRolesForCreateOrUpdateUser(dto.getRoles());
                    user.setRoles(newRoles);
                    updated = true;
                }
            }
        }
        if (!updated) {
            return UserDto.of(user);
        }

        User updatedUser = userCrudService.save(user);
        // this initialization we are doing in order to avoid Lazy initialization exception
        // because the oldUser is copyed form the original user and hibernate did not know about that
        // and oldUser is not in the hibernate session...
        Hibernate.initialize(oldUser.getRoles());
        Hibernate.initialize(updatedUser.getRoles());
        userLogService.logUpdatingEvent(oldUser, user);
        return UserDto.of(updatedUser);
    }

    @Override
    public void deleteUser(Long id) throws UserNotFoundException {
        User user = userCrudService.findById(id);
        userCrudService.delete(user);
        userLogService.logDeletingEvent(user);
    }

    @Override
    public ListResponseDto<DeleteResponseDto> deleteUsers(List<Long> ids) {
        return ListResponseDto.of(ids.stream().map(id -> {
            try {
                deleteUser(id);
                return DeleteResponseDto.of(id);
            } catch (UserNotFoundException e) {
                log.error("Error when delete user with id: {} from list users: {}", id, ids, e);
                return DeleteResponseDto.of(id, e.getError());
            } catch (DataIntegrityViolationException e) {
                log.error("Error when delete user with id: {} from list users: {}. Can't delete, sql error", id, ids, e);
                return DeleteResponseDto.of(id, ErrorResponse.of(Error.DEPENDENCIES_ERROR));
            } catch (Exception e) {
                log.error("Error when delete user with id: {} from list users", id, e);
                return DeleteResponseDto.of(id, ErrorResponse.of(Error.SERVER_ERROR));
            }
        }).collect(Collectors.toList()));
    }

    @Override
    @Transactional
    public PageResponseDto<UserDto> getUserList(PageRequestDto pageRequestDto) {
        Page<User> userPage = userCrudService.findByFilter(pageRequestDto);
        List<UserDto> userDtos = userPage.getContent()
                .stream()
                .map(UserDto::of)
                .collect(Collectors.toList());

        return PageResponseDto.of(userDtos, userPage.getTotalElements());
    }

    private void checkLoginExist(String login) throws UserAlreadyExistsException {
        Optional<User> optUserByLogin = userCrudService.findByLogin(login);
        if (optUserByLogin.isPresent()) {
            throw new UserAlreadyExistsException(Error.LOGIN_ALREADY_EXISTS,
                    "User already exists with login: " + login.trim());
        }
    }

    private void checkEmailExist(String email) throws UserAlreadyExistsException {
        Optional<User> optUserByEmail = userCrudService.findByEmail(email);
        if (optUserByEmail.isPresent()) {
            throw new UserAlreadyExistsException(Error.EMAIL_ALREADY_EXISTS,
                    "User already exists with login: " + email.trim());
        }
    }

    private Set<Role> getRolesForCreateOrUpdateUser(Set<Long> roleIds) throws UserCreationException {
        if (CollectionUtils.isEmpty(roleIds)) {
            return new HashSet<>();
        }
        Set<Role> roles = roleCrudService.findByIds(roleIds);
        //roles not found by ids
        if (roles.isEmpty() || roles.size() != roleIds.size()) {
            throw new UserCreationException(Error.ROLE_NOT_FOUND, "Roles not found");
        }
        return roles;
    }

    private User deepCopy(User user) {
        User copyUser = serializationUtil.deepCopy(user, User.class);
        copyUser.setRoles(serializationUtil.deepCopySet(user.getRoles(), Role.class));
        return copyUser;
    }
}

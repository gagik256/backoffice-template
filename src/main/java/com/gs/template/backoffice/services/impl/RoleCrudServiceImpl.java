package com.gs.template.backoffice.services.impl;

import com.querydsl.core.types.Predicate;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.QRole;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.repositories.jpa.QPredicates;
import com.gs.template.backoffice.repositories.jpa.RoleRepository;
import com.gs.template.backoffice.services.RoleCrudService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleCrudServiceImpl implements RoleCrudService {
    private final RoleRepository roleRepository;

    @Override
    public Optional<Role> findByName(String name) {
        return roleRepository.findOne(QRole.role.name.eq(name.trim()));
    }

    @Override
    public Role save(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role saveAndFlush(Role role) {
        return roleRepository.saveAndFlush(role);
    }

    @Override
    public Role findById(Long id) throws RoleNotFoundException {
        return roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException(id));
    }

    @Override
    public Page<Role> findByFilter(PageRequestDto pageRequestDto) {
        Predicate predicate = QPredicates.toRolePredicate(pageRequestDto.getFilters());
        return roleRepository.findAll(predicate, pageRequestDto.asPageRequest());
    }

    @Override
    public void delete(Role role) {
        roleRepository.delete(role);
    }

    @Override
    public Set<Role> findByIds(Set<Long> ids) {
        Set<Role> roles = new HashSet<>();
        if (!CollectionUtils.isEmpty(ids)) {
            roleRepository.findAll(QRole.role.id.in(ids)).forEach(roles::add);
        }
        return roles;
    }
}



package com.gs.template.backoffice.services.log.impl;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.dto.log.PermissionChangeDto;
import com.gs.template.backoffice.dto.log.RoleLogDto;
import com.gs.template.backoffice.dto.log.UserChangeDto;
import com.gs.template.backoffice.dto.user.ShortUserDto;
import com.gs.template.backoffice.model.entities.elastic.log.*;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.repositories.elastic.log.ElasticDao;
import com.gs.template.backoffice.repositories.elastic.log.ElasticQBuilder;
import com.gs.template.backoffice.repositories.elastic.log.RoleLogElasticRepository;
import com.gs.template.backoffice.services.log.RoleLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleLogServiceImpl implements RoleLogService {

    public static final String ROLE_LOG_INDEX = "role_log";

    private final RoleLogElasticRepository roleLogRepository;
    private final ElasticDao<RoleLog> elasticDao;

    @Override
    @Transactional
    public void logCreatingEvent(Role role) {
        RoleLog roleLog = RoleLog.builder()
                .eventType(LogEventType.CREATE)
                .roleChangedFields(RoleChangedFields.builder()
                        .nameOldValue(role.getName())
                        .descriptionOldValue(role.getDescription())
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        RoleChangedFields roleChangedFields = roleLog.getRoleChangedFields();
        roleChangedFields.setUserChanges(buildUserChanges(role, ChangedType.ADDED));
        roleChangedFields.setPermissionChanges(buildPermissionChanges(role, ChangedType.ADDED));
        roleLogRepository.save(roleLog);
    }

    @Override
    @Transactional
    public void logUpdatingEvent(Role oldRole, Role newRole) {
        if (oldRole == null) {
            return;
        }
        RoleLog roleLog = RoleLog.builder()
                .eventType(LogEventType.UPDATE)
                .roleChangedFields(RoleChangedFields.builder()
                        .nameOldValue(oldRole.getName())
                        .nameNewValue(newRole.getName())
                        .descriptionOldValue(oldRole.getDescription())
                        .descriptionNewValue(newRole.getDescription())
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        RoleChangedFields roleChangedFields = roleLog.getRoleChangedFields();
        roleChangedFields.setUserChanges(buildUserChangesForUpdate(oldRole, newRole));
        roleChangedFields.setPermissionChanges(buildPermissionChangesForUpdate(oldRole, newRole));
        roleLogRepository.save(roleLog);
    }

    @Override
    @Transactional
    public void logDeletingEvent(Role role) {
        RoleLog roleLog = RoleLog.builder()
                .eventType(LogEventType.DELETE)
                .roleChangedFields(RoleChangedFields.builder()
                        .nameOldValue(role.getName())
                        .descriptionOldValue(role.getDescription())
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        RoleChangedFields roleChangedFields = roleLog.getRoleChangedFields();
        roleChangedFields.setUserChanges(buildUserChanges(role, ChangedType.DELETED));
        roleChangedFields.setPermissionChanges(buildPermissionChanges(role, ChangedType.DELETED));
        roleLogRepository.save(roleLog);
    }

    @Override
    @Transactional
    public PageResponseDto<RoleLogDto> getRolesLogs(PageRequestDto pageRequestDto) {
        QueryBuilder query = ElasticQBuilder.toRoleLogQuery(pageRequestDto);
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(query).setPageable(pageRequestDto.asPageRequest());
        List<RoleLog> logs = elasticDao.findByQuery(nativeSearchQuery, RoleLog.class, ROLE_LOG_INDEX);

        List<RoleLogDto> roleLogDtos = new ArrayList<>();
        for (RoleLog roleLog : logs) {
            roleLogDtos.add(new RoleLogDto(roleLog,
                    convertUserChangeDto(roleLog.getRoleChangedFields().getUserChanges()),
                    convertPermissionChangeDto(roleLog.getRoleChangedFields().getPermissionChanges())));
        }
        return PageResponseDto.of(roleLogDtos, roleLogDtos.size());
    }

    private List<UserChangeDto> convertUserChangeDto(Set<UserChange> userChanges) {
        List<UserChangeDto> userChangeDtos = new ArrayList<>();
        for (UserChange userChange : userChanges) {
            userChangeDtos.add(
                    UserChangeDto.builder()
                            .changedType(userChange.getChangedType())
                            .changedUser(userChange.getUser())
                            .build());
        }
        return userChangeDtos;
    }

    private List<PermissionChangeDto> convertPermissionChangeDto(Set<PermissionChange> permissionChanges) {
        List<PermissionChangeDto> permissionChangeDtos = new ArrayList<>();
        for (PermissionChange permissionChange : permissionChanges) {
            permissionChangeDtos.add(
                    PermissionChangeDto.builder()
                            .changedType(permissionChange.getChangedType())
                            .changedPermission(permissionChange.getPermission())
                            .build());
        }
        return permissionChangeDtos;
    }

    private Set<UserChange> buildUserChanges(Role role, ChangedType type) {
        Set<UserChange> userChanges = new HashSet<>();
        Set<User> users = role.getUsers();
        addToUserChanges(userChanges, users, type);
        return userChanges;
    }

    private Set<UserChange> buildUserChangesForUpdate(Role oldRole, Role newRole) {
        Set<UserChange> userChanges = new HashSet<>();
        Set<User> newUsers = newRole.getUsers();
        Set<User> oldUsers = oldRole.getUsers();
        // imitableUsers is the users that did not changed during role update
        Set<User> imitableUsers = oldRole.getUsers();

        newUsers.removeAll(oldUsers);
        imitableUsers.removeAll(newUsers);
        oldUsers.removeAll(newUsers);

        addToUserChanges(userChanges, newUsers, ChangedType.ADDED);
        addToUserChanges(userChanges, imitableUsers, ChangedType.IMMUTABLE);
        addToUserChanges(userChanges, oldUsers, ChangedType.DELETED);
        return userChanges;
    }

    private void addToUserChanges(Set<UserChange> userChanges, Set<User> users, ChangedType type) {
        users.forEach(user -> {
            userChanges.add(UserChange.builder()
                    .changedType(type)
                    .user(ShortUserDto.of(user))
                    .build());
        });
    }

    private Set<PermissionChange> buildPermissionChanges(Role role, ChangedType type) {
        Set<PermissionChange> permissionChanges = new HashSet<>();
        Set<Permission> permissions = role.getPermissions();

        addToPermissionChanges(permissionChanges, permissions, type);
        return permissionChanges;
    }

    private Set<PermissionChange> buildPermissionChangesForUpdate(Role oldRole, Role newRole) {
        Set<PermissionChange> permissionChanges = new HashSet<>();
        Set<Permission> newPermission = newRole.getPermissions();
        Set<Permission> oldPermission = oldRole.getPermissions();
        // imitablePermissions is the permissions that did not changed during role update
        Set<Permission> imitablePermissions = oldRole.getPermissions();

        newPermission.removeAll(oldPermission);
        oldPermission.removeAll(newPermission);

        addToPermissionChanges(permissionChanges, newPermission, ChangedType.ADDED);
        addToPermissionChanges(permissionChanges, imitablePermissions, ChangedType.IMMUTABLE);
        addToPermissionChanges(permissionChanges, oldPermission, ChangedType.DELETED);
        return permissionChanges;
    }

    private void addToPermissionChanges(Set<PermissionChange> permissionChanges, Set<Permission> permissions, ChangedType type) {
        permissions.forEach(permission -> permissionChanges.add(PermissionChange.builder()
                .changedType(type)
                .permission(PermissionDto.of(permission))
                .build()));
    }
}

package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.User;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.Set;

public interface UserCrudService {

    User save(User user);

    User findById(Long id) throws UserNotFoundException;

    void delete(User user);

    Optional<User> findByLogin(String login);

    Optional<User> findByEmail(String email);

    Page<User> findByFilter(PageRequestDto pageRequestDto);

    Set<User> findByIds(Set<Long> userIds);
}

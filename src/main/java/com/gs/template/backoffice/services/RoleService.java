package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.role.CreateRoleDto;
import com.gs.template.backoffice.dto.role.RoleDto;
import com.gs.template.backoffice.dto.role.UpdateRoleDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.RoleAlreadyExistException;
import com.gs.template.backoffice.errors.api.RoleCreationException;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;

import javax.validation.ValidationException;
import java.util.List;

public interface RoleService {

    RoleDto createRole(CreateRoleDto createRoleDto)
            throws RoleAlreadyExistException, ValidationException, RoleCreationException;

    RoleDto updateRole(UpdateRoleDto updateRoleDto, Long id)
            throws RoleNotFoundException, ValidationException, RoleCreationException;

    void deleteRole(Long id) throws RoleNotFoundException, ValidationException;

    ListResponseDto<DeleteResponseDto> deleteRoles(List<Long> ids);

    PageResponseDto<RoleDto> getRoleList(PageRequestDto pageRequestDto) throws ApiException;
}

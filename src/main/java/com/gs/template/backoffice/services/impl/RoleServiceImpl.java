package com.gs.template.backoffice.services.impl;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.role.CreateRoleDto;
import com.gs.template.backoffice.dto.role.RoleDto;
import com.gs.template.backoffice.dto.role.UpdateRoleDto;
import com.gs.template.backoffice.errors.api.RoleAlreadyExistException;
import com.gs.template.backoffice.errors.api.RoleCreationException;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.model.api.Error;
import com.gs.template.backoffice.model.api.ErrorResponse;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.services.PermissionService;
import com.gs.template.backoffice.services.RoleCrudService;
import com.gs.template.backoffice.services.RoleService;
import com.gs.template.backoffice.services.UserCrudService;
import com.gs.template.backoffice.services.log.RoleLogService;
import com.gs.template.backoffice.utils.SerializationUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleCrudService roleCrudService;
    private final UserCrudService userCrudService;
    private final PermissionService permissionService;
    private final RoleLogService roleLogService;
    private final SerializationUtil serializationUtil;

    @Override
    @Transactional
    public RoleDto createRole(CreateRoleDto roleDto) throws RoleAlreadyExistException, RoleCreationException {
        checkNameExist(roleDto);

        Set<Permission> permissions = getPermissionsForCreateRole(roleDto.getPermissions());
        Set<User> users = getUsersForCreateOrUpdateRole(roleDto.getUsers());

        Role role = roleCrudService.save(new Role(roleDto.getName().trim(),
                roleDto.getDescription(),
                permissions,
                users));
        roleLogService.logCreatingEvent(role);
        return RoleDto.of(role);
    }

    private void checkNameExist(CreateRoleDto roleDto) throws RoleAlreadyExistException {
        Optional<Role> roleFromDb = roleCrudService.findByName(roleDto.getName());
        if (roleFromDb.isPresent()) {
            throw new RoleAlreadyExistException(roleDto.getName().trim());
        }
    }

    @Override
    @Transactional
    public RoleDto updateRole(UpdateRoleDto updateRoleDto, Long id)
            throws RoleNotFoundException, RoleCreationException {
        boolean updated = false;
        Role role = roleCrudService.findById(id);
        Role oldRole = deepCopy(role);

        boolean updatedPermissions = updatePermissionsInRole(updateRoleDto.getPermissions(), role);
        boolean updatedUsers = updateUsersInRole(updateRoleDto.getUsers(), role);

        if (StringUtils.isNotBlank(updateRoleDto.getDescription()) && !updateRoleDto.getDescription()
                .equals(role.getDescription())) {
            role.setDescription(updateRoleDto.getDescription());
            updated = true;
        }

        if (!updated && !updatedPermissions && !updatedUsers) {
            return RoleDto.of(role);
        }
        roleCrudService.saveAndFlush(role);
        // this initialization we are doing in order to avoid Lazy initialization exception
        // because the oldRole is copyed form the original role and hibernate did not know about that
        // and oldRole is not in the hibernate session...
        Hibernate.initialize(oldRole.getUsers());
        Hibernate.initialize(oldRole.getPermissions());
        Hibernate.initialize(role.getUsers());
        Hibernate.initialize(role.getPermissions());
        roleLogService.logUpdatingEvent(oldRole, role);
        return RoleDto.of(role);
    }

    private boolean updateUsersInRole(Set<Long> userIds, Role role) throws RoleCreationException {
        if (userIds == null) {
            return false;
        }
        Set<User> roleUsers = role.getUsers();
        if (userIds.isEmpty()) {
            if (!roleUsers.isEmpty()) {
                role.setUsers(new HashSet<>());
                return true;
            }
        } else {
            Set<User> users = getUsersForCreateOrUpdateRole(userIds);
            if (!roleUsers.equals(users)) {
                role.setUsers(users);
                return true;
            }
        }
        return false;
    }

    private boolean updatePermissionsInRole(Set<String> permissionNames, Role role) throws RoleCreationException {
        if (permissionNames == null) {
            return false;
        }
        if (permissionNames.isEmpty()) {
            if (!role.getPermissions().isEmpty()) {
                role.setPermissions(Collections.emptySet());
                return true;
            }
        } else {
            permissionNames = getNotBlankPermissionNames(permissionNames);

            Set<String> rolePermissionNames = role.getPermissions().stream().map(Permission::getName)
                    .collect(Collectors.toSet());
            if (!rolePermissionNames.equals(permissionNames)) {
                Set<Permission> permissions = permissionService.findAllPermissionsByNames(permissionNames);
                role.setPermissions(permissions);
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public void deleteRole(Long id) throws RoleNotFoundException {
        Role role = roleCrudService.findById(id);
        // this initialization we are doing in order to avoid Lazy initialization exception
        // because the role is deleted and hibernate did not know about that
        // and role is not in the hibernate session...
        Hibernate.initialize(role.getUsers());
        Hibernate.initialize(role.getPermissions());
        roleCrudService.delete(role);
        roleLogService.logDeletingEvent(role);
    }

    @Override
    public ListResponseDto<DeleteResponseDto> deleteRoles(List<Long> ids) {
        return ListResponseDto.of(ids.stream().map(id -> {
            try {
                deleteRole(id);
                return DeleteResponseDto.of(id);
            } catch (RoleNotFoundException e) {
                log.error("Error when delete role with id: {} from list roless: {}", id, ids, e);
                return DeleteResponseDto.of(id, e.getError());
            } catch (DataIntegrityViolationException e) {
                log.error("Error when delete role with id: {} from list roles: {}. Can't delete, sql error", id, ids,
                        e);
                return DeleteResponseDto.of(id, ErrorResponse.of(Error.DEPENDENCIES_ERROR));
            } catch (Exception e) {
                log.error("Error when delete role with id: {} from list roles: {}", id, ids, e);
                return DeleteResponseDto.of(id, ErrorResponse.of(Error.SERVER_ERROR));
            }
        }).collect(toList()));
    }

    @Override
    @Transactional
    public PageResponseDto<RoleDto> getRoleList(PageRequestDto pageRequestDto) {
        Page<Role> rolePage = roleCrudService.findByFilter(pageRequestDto);
        List<Role> rolePageContent = rolePage.getContent();
        List<RoleDto> roleDtos = rolePageContent.stream()
                .map(RoleDto::of)
                .collect(toList());
        return PageResponseDto.of(roleDtos, rolePage.getTotalElements());
    }

    private Set<Permission> getPermissionsForCreateRole(Set<String> permissionNames)
            throws RoleCreationException {
        if (CollectionUtils.isEmpty(permissionNames)) {
            return new HashSet<>();
        }
        permissionNames = getNotBlankPermissionNames(permissionNames);

        Set<Permission> permissions = permissionService.findAllPermissionsByNames(permissionNames);

        if (!CollectionUtils.isEmpty(permissionNames) && permissionNames.size() != permissions.size()) {
            throw new RoleCreationException(Error.PERMISSION_NOT_FOUND, "Permission not found");
        }
        return permissions;
    }

    private Set<User> getUsersForCreateOrUpdateRole(Set<Long> userIds) throws RoleCreationException {
        if (CollectionUtils.isEmpty(userIds)) {
            return new HashSet<>();
        }
        Set<User> users = userCrudService.findByIds(userIds);
        if (!CollectionUtils.isEmpty(userIds) && users.size() != userIds.size()) {
            throw new RoleCreationException(Error.USER_NOT_FOUND, "Users not found");
        }
        return users;
    }

    private Set<String> getNotBlankPermissionNames(Set<String> permissionNames) throws RoleCreationException {
        Set<String> notBlankPermissionNames = permissionNames.stream().filter(StringUtils::isNotBlank)
                .collect((Collectors.toSet()));
        if (notBlankPermissionNames.isEmpty()) {
            throw new RoleCreationException(Error.PERMISSION_NOT_FOUND, "Permission not found");
        }
        return notBlankPermissionNames;
    }

    private Role deepCopy(Role role) {
        Role copyRole = serializationUtil.deepCopy(role, Role.class);
        copyRole.setUsers(serializationUtil.deepCopySet(role.getUsers(), User.class));
        return copyRole;
    }
}

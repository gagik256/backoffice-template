package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.model.entities.jpa.Permission;

import java.util.Set;

public interface PermissionService {

    PageResponseDto<PermissionDto> getPermissionsDto();

    Set<Permission> findAllPermissionsByNames(Set<String> permissionNames);
}

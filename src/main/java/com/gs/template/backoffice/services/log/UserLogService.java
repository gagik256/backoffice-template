package com.gs.template.backoffice.services.log;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.log.UserLogDto;
import com.gs.template.backoffice.model.entities.jpa.User;

public interface UserLogService {

    /**
     * Add user create log
     * @param user
     */
    void logCreatingEvent(User user);

    /**
     * Add user update log
     * @param oldUser
     * @param newUser
     */
    void logUpdatingEvent(User oldUser, User newUser);

    /**
     * Add user delete log
     * @param user
     */
    void logDeletingEvent(User user);

    /**
     * Find all user log that matches specified filters
     * @param requestDto
     * @return
     */
    PageResponseDto<UserLogDto> getUsersLogs(PageRequestDto requestDto);
}

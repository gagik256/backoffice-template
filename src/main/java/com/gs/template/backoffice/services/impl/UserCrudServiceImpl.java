package com.gs.template.backoffice.services.impl;

import com.querydsl.core.types.Predicate;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.QUser;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.repositories.jpa.QPredicates;
import com.gs.template.backoffice.repositories.jpa.UserRepository;
import com.gs.template.backoffice.services.UserCrudService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserCrudServiceImpl implements UserCrudService {

    private final UserRepository userRepository;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User findById(Long id) throws UserNotFoundException {
        User user = userRepository.findOne(QUser.user.id.eq(id))
                .orElseThrow(() -> new UserNotFoundException(id));
        Hibernate.initialize(user.getRoles());
        return user;
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return userRepository.findOne(QUser.user.login.eq(login.trim()));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findOne(QUser.user.email.eq(email.trim().toLowerCase()));
    }

    @Override
    public Page<User> findByFilter(PageRequestDto pageRequestDto) {
        Predicate predicate = QPredicates.toUserPredicate(pageRequestDto.getFilters());
        return userRepository.findAll(predicate, pageRequestDto.asPageRequest());
    }

    @Override
    public Set<User> findByIds(Set<Long> userIds) {
        Set<User> users = new HashSet<>();
        if (!CollectionUtils.isEmpty(userIds)) {
            userRepository.findAll(QUser.user.id.in(userIds)).forEach(users::add);
        }
        return users;
    }
}

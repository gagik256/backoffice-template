package com.gs.template.backoffice.services;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.model.entities.jpa.Role;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.Set;

public interface RoleCrudService {
    Optional<Role> findByName(String name);

    Role save(Role role);

    Role saveAndFlush(Role role);

    Role findById(Long id) throws RoleNotFoundException;

    Page<Role> findByFilter(PageRequestDto pageRequestDto);

    void delete(Role role);

    Set<Role> findByIds(Set<Long> ids) ;
}

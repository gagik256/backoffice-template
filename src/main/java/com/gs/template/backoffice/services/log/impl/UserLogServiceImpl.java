package com.gs.template.backoffice.services.log.impl;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.log.UserLogDto;
import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import com.gs.template.backoffice.model.entities.elastic.log.UserChangedFields;
import com.gs.template.backoffice.model.entities.elastic.log.UserLog;
import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import com.gs.template.backoffice.repositories.elastic.log.ElasticDao;
import com.gs.template.backoffice.repositories.elastic.log.ElasticQBuilder;
import com.gs.template.backoffice.repositories.elastic.log.UserLogElasticRepository;
import com.gs.template.backoffice.services.log.UserLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserLogServiceImpl implements UserLogService {

    public static final String USER_LOG_INDEX = "user_log";

    private final UserLogElasticRepository userLogRepository;
    private final ElasticDao<UserLog> elasticDao;

    @Override
    @Transactional
    public void logCreatingEvent(User user) {
        UserLog userLog = UserLog.builder()
                .eventType(LogEventType.CREATE)
                .userChangedFields(UserChangedFields.builder()
                        .loginOldValue(user.getLogin())
                        .firstNameOldValue(user.getFirstName())
                        .lastNameOldValue(user.getLastName())
                        .countryOldValue(user.getCountry())
                        .emailOldValue(user.getEmail())
                        .rolesOldValue(getJoinedRoleNames(user))
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        userLogRepository.save(userLog);
    }

    @Override
    public void logUpdatingEvent(User oldUser, User newUser) {
        if (oldUser == null) {
            return;
        }
        UserLog userLog = UserLog.builder()
                .eventType(LogEventType.UPDATE)
                .userChangedFields(UserChangedFields.builder()
                        .loginOldValue(oldUser.getLogin())
                        .loginNewValue(newUser.getLogin())
                        .firstNameOldValue(oldUser.getFirstName())
                        .firstNameNewValue(newUser.getFirstName())
                        .lastNameOldValue(oldUser.getLastName())
                        .lastNameNewValue(newUser.getLastName())
                        .countryOldValue(oldUser.getCountry())
                        .countryNewValue(newUser.getCountry())
                        .emailOldValue(oldUser.getEmail())
                        .emailNewValue(newUser.getEmail())
                        .rolesOldValue(getJoinedRoleNames(oldUser))
                        .rolesNewValue(getJoinedRoleNames(newUser))
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        userLogRepository.save(userLog);
    }

    @Override
    @Transactional
    public void logDeletingEvent(User user) {
        UserLog userLog = UserLog.builder()
                .eventType(LogEventType.DELETE)
                .userChangedFields(UserChangedFields.builder()
                        .loginOldValue(user.getLogin())
                        .firstNameOldValue(user.getFirstName())
                        .lastNameOldValue(user.getLastName())
                        .countryOldValue(user.getCountry())
                        .emailOldValue(user.getEmail())
                        .rolesOldValue(getJoinedRoleNames(user))
                        .build())
                .createdAt(System.currentTimeMillis())
                .build();
        userLogRepository.save(userLog);
    }

    @Override
    @Transactional
    public PageResponseDto<UserLogDto> getUsersLogs(PageRequestDto pageRequestDto) {
        QueryBuilder query = ElasticQBuilder.toUserLogQuery(pageRequestDto);
        NativeSearchQuery nativeSearchQuery = new NativeSearchQuery(query).setPageable(pageRequestDto.asPageRequest());
        List<UserLog> logs = elasticDao.findByQuery(nativeSearchQuery, UserLog.class, USER_LOG_INDEX);

        List<UserLogDto> userLogDtos = logs.stream()
                .map(UserLogDto::new)
                .collect(Collectors.toList());
        return PageResponseDto.of(userLogDtos, userLogDtos.size());
    }

    private String getJoinedRoleNames(User user) {
        return user.getRoles().stream().map(Role::getName).collect(Collectors.joining(", "));
    }
}

package com.gs.template.backoffice.services.impl;

import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.model.entities.jpa.Permission;
import com.gs.template.backoffice.model.entities.jpa.QPermission;
import com.gs.template.backoffice.repositories.jpa.PermissionRepository;
import com.gs.template.backoffice.services.PermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PermissionServiceImpl implements PermissionService {

    private static final Sort PERM_DEFAULT_SORT = Sort.by(Sort.Direction.ASC, "name");

    private final PermissionRepository permissionRepository;

    @Override
    public PageResponseDto<PermissionDto> getPermissionsDto() {
        List<PermissionDto> dtos = permissionRepository.findAll(PERM_DEFAULT_SORT).stream()
                .map(PermissionDto::of)
                .collect(Collectors.toList());
        return PageResponseDto.of(dtos, dtos.size());
    }

    @Override
    public Set<Permission> findAllPermissionsByNames(Set<String> permissionNames) {
        Set<Permission> permissions = new HashSet<>();
        if (!CollectionUtils.isEmpty(permissionNames)) {
            permissionRepository.findAll(QPermission.permission.name.in(permissionNames))
                    .forEach(permissions::add);
        }
        return permissions;
    }
}

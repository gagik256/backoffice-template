package com.gs.template.backoffice.services.log;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.log.RoleLogDto;
import com.gs.template.backoffice.model.entities.jpa.Role;

public interface RoleLogService {

    /**
     * Add role create log
     * @param role
     */
    void logCreatingEvent(Role role);

    /**
     * Add role update log
     * @param oldRole
     * @param newRole
     */
    void logUpdatingEvent(Role oldRole, Role newRole);

    /**
     * Add role delete log
     * @param role
     */
    void logDeletingEvent(Role role);

    /**
     * Find all role log that matches specified filters
     * @param requestDto
     * @return
     */
    PageResponseDto<RoleLogDto> getRolesLogs(PageRequestDto requestDto);

}

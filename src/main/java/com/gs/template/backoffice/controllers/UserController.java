package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.user.CreateUserDto;
import com.gs.template.backoffice.dto.user.UpdateUserDto;
import com.gs.template.backoffice.dto.user.UserDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.UserAlreadyExistsException;
import com.gs.template.backoffice.errors.api.UserCreationException;
import com.gs.template.backoffice.errors.api.UserNotFoundException;
import com.gs.template.backoffice.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
@Validated
public class UserController {

    private final UserService userService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody @Valid CreateUserDto userCreateDto)
            throws UserAlreadyExistsException, UserCreationException {
        log.info("Users#create - dto: {}", userCreateDto);
        UserDto userDto = userService.createUser(userCreateDto);
        return ResponseEntity.ok(userDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) throws UserNotFoundException {
        log.info("Users#delete - id: {}", id);
        userService.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUsers(@RequestBody @Valid @NotEmpty List<Long> ids) {
        log.info("Users#deleteUsers");
        ListResponseDto<DeleteResponseDto> response = userService.deleteUsers(ids);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUsers(@RequestBody @Valid PageRequestDto requestDto) throws ApiException {
        log.info("Users#getUsers - request: {}", requestDto);
        PageResponseDto<UserDto> usersDto = userService.getUserList(requestDto);
        return ResponseEntity.ok(usersDto);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody UpdateUserDto updateDto)
            throws UserNotFoundException, UserAlreadyExistsException, UserCreationException {
        log.info("Users#update - id: {}, dto: {}", id, updateDto);
        UserDto userDto = userService.updateUser(id, updateDto);
        return ResponseEntity.ok(userDto);
    }
}

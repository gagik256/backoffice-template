package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.log.RoleLogDto;
import com.gs.template.backoffice.dto.log.UserLogDto;
import com.gs.template.backoffice.services.log.RoleLogService;
import com.gs.template.backoffice.services.log.UserLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/v1/logs", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
public class LogEventController {

    private final UserLogService userLogService;
    private final RoleLogService roleLogService;

    @PostMapping(value = "/user/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getUserLogs(@RequestBody @Valid PageRequestDto requestDto) {
        log.info("User Logs#getUserLogs - request: {}", requestDto);
        PageResponseDto<UserLogDto> usersLogDto = userLogService.getUsersLogs(requestDto);
        return ResponseEntity.ok(usersLogDto);
    }

    @PostMapping(value = "/role/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRoleLogs(@RequestBody @Valid PageRequestDto requestDto) {
        log.info("Role Logs#getRoleLogs - request: {}", requestDto);
        PageResponseDto<RoleLogDto> rolesLogDto = roleLogService.getRolesLogs(requestDto);
        return ResponseEntity.ok(rolesLogDto);
    }
}

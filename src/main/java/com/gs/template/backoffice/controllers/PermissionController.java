package com.gs.template.backoffice.controllers;

import com.gs.template.backoffice.services.PermissionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1/permissions", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
public class PermissionController {
    private final PermissionService permissionsService;

    @GetMapping(value = "/list")
    public ResponseEntity<?> getPermissions() {
        log.info("Permissions#list");
        return ResponseEntity.ok(permissionsService.getPermissionsDto());
    }
}



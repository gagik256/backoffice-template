package com.gs.template.backoffice.controllers;


import com.gs.template.backoffice.dto.DeleteResponseDto;
import com.gs.template.backoffice.dto.ListResponseDto;
import com.gs.template.backoffice.dto.PageRequestDto;
import com.gs.template.backoffice.dto.PageResponseDto;
import com.gs.template.backoffice.dto.role.CreateRoleDto;
import com.gs.template.backoffice.dto.role.RoleDto;
import com.gs.template.backoffice.dto.role.UpdateRoleDto;
import com.gs.template.backoffice.errors.api.ApiException;
import com.gs.template.backoffice.errors.api.RoleAlreadyExistException;
import com.gs.template.backoffice.errors.api.RoleCreationException;
import com.gs.template.backoffice.errors.api.RoleNotFoundException;
import com.gs.template.backoffice.services.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/roles", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
@Slf4j
@Validated
public class RoleController {
    private final RoleService roleService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createRole(@RequestBody @Valid CreateRoleDto createRoleDto)
            throws RoleAlreadyExistException, RoleCreationException {
        log.info("Roles#create - dto: {}", createRoleDto);
        RoleDto roleDto = roleService.createRole(createRoleDto);
        return ResponseEntity.ok(roleDto);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateRole(@RequestBody @Valid UpdateRoleDto updateRoleDto, @PathVariable Long id)
            throws RoleNotFoundException, RoleCreationException {
        log.info("Roles#update - dto: {}", updateRoleDto);
        RoleDto roleDto = roleService.updateRole(updateRoleDto, id);
        return ResponseEntity.ok(roleDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRole(@PathVariable Long id)
            throws RoleNotFoundException {
        log.info("Roles#delete - id: {}", +id);
        roleService.deleteRole(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "delete", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteRoles(@RequestBody @Valid @NotEmpty List<Long> ids) {
        log.info("Users#deleteUsers");
        ListResponseDto<DeleteResponseDto> response = roleService.deleteRoles(ids);
        return ResponseEntity.ok(response);
    }

    @PostMapping(value = "/list", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getRoles(@RequestBody @Valid PageRequestDto requestDto) throws ApiException {
        log.info("Roles#list - dto: {}", requestDto);
        PageResponseDto<RoleDto> rolesDto = roleService.getRoleList(requestDto);
        return ResponseEntity.ok(rolesDto);
    }
}

package com.gs.template.backoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.time.ZoneOffset;
import java.util.TimeZone;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.gs.template.backoffice.repositories.jpa")
@EnableElasticsearchRepositories(basePackages = "com.gs.template.backoffice.repositories.elastic")
public class Application {

    static {
        System.setProperty("user.timezone", "UTC");
        TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC.getId()));
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}

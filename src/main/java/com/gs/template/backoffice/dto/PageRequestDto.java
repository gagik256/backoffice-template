package com.gs.template.backoffice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gs.template.backoffice.dto.filter.FilterDto;
import com.gs.template.backoffice.dto.filter.SortDto;
import com.gs.template.backoffice.dto.filter.SortDto.Order;
import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import javax.annotation.Nullable;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Data
public class PageRequestDto {

    @Min(0L)
    private int page = 0;
    @Positive
    private int pageSize = 20;
    @Nullable
    private SortDto sort;
    private List<FilterDto> filters = new ArrayList<>();

    @JsonIgnore
    public PageRequest asPageRequest() {
        return asPageRequest(Function.identity());
    }

    @JsonIgnore
    public PageRequest asPageRequest(Function<String, String> fieldNameMapper) {
        if (sort == null) {
            return PageRequest.of(page, pageSize);
        } else {
            Sort.Direction direction = sort.getOrder() == Order.ASC ? Sort.Direction.ASC : Sort.Direction.DESC;
            Sort.Order order = new Sort.Order(direction, fieldNameMapper.apply(sort.getField()));
            return PageRequest.of(page, pageSize, Sort.by(order));
        }
    }
}

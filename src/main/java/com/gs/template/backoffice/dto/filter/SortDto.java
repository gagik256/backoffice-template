package com.gs.template.backoffice.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class SortDto {

    private static final Map<String, Order> ORDER_MAP = Arrays.stream(Order.values())
            .collect(Collectors.toMap(Enum::name, Function.identity()));

    @NotBlank
    private String field;
    private Order order = Order.ASC;

    public enum Order {
        ASC, DESC;

        @JsonCreator
        public static Order fromString(String key) {
            return key == null ? Order.ASC : ORDER_MAP.getOrDefault(key.toUpperCase(), Order.ASC);
        }
    }
}

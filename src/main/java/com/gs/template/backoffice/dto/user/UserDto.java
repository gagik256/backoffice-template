package com.gs.template.backoffice.dto.user;

import com.gs.template.backoffice.model.entities.jpa.Role;
import com.gs.template.backoffice.model.entities.jpa.User;
import lombok.Value;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Value
public class UserDto {
    long id;
    String login;
    String email;
    String firstName;
    String lastName;
    String country;
    Long createdAt;
    Long updatedAt;
    Set<String> roles;

    public static UserDto of(User user) {
        Set<String> roles = new HashSet<>();
        if (!CollectionUtils.isEmpty(user.getRoles())) {
            roles = user.getRoles().stream().map(Role::getName).collect(Collectors.toSet());
        }
        return new UserDto(user.getId(), user.getLogin(), user.getEmail(),
                user.getFirstName(), user.getLastName(), user.getCountry(), user.getCreatedAt(), user.getUpdatedAt(), roles);
    }
}

package com.gs.template.backoffice.dto.role;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateRoleDto {
    private String description;
    private Set<String> permissions;
    private Set<Long> users;
}

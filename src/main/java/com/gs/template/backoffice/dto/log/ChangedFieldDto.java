package com.gs.template.backoffice.dto.log;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChangedFieldDto {
    private String name;
    private String oldValue;
    private String newValue;
}

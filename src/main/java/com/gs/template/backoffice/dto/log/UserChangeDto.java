package com.gs.template.backoffice.dto.log;

import com.gs.template.backoffice.dto.user.ShortUserDto;
import com.gs.template.backoffice.model.entities.elastic.log.ChangedType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserChangeDto {
    private ChangedType changedType;
    private ShortUserDto changedUser;
}

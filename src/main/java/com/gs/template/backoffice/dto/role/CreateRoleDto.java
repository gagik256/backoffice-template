package com.gs.template.backoffice.dto.role;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
public class CreateRoleDto {
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    private Set<String> permissions;
    private Set<Long> users;
}

package com.gs.template.backoffice.dto;

import com.gs.template.backoffice.model.api.ErrorResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class DeleteResponseDto {
    private final Long id;
    private final ErrorResponseDto error;

    public static com.gs.template.backoffice.dto.DeleteResponseDto of(Long id, ErrorResponse errorResponse) {
        return new com.gs.template.backoffice.dto.DeleteResponseDto(id, ErrorResponseDto.of(errorResponse));
    }
    public static com.gs.template.backoffice.dto.DeleteResponseDto of(Long id) {
        return new com.gs.template.backoffice.dto.DeleteResponseDto(id, null);
    }
}

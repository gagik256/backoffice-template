package com.gs.template.backoffice.dto.user;

import com.gs.template.backoffice.model.entities.jpa.User;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ShortUserDto {
    private final long id;
    private final String login;
    private final String email;
    private final String firstName;
    private final String lastName;

    public static ShortUserDto of(User user) {
        return new ShortUserDto(user.getId(), user.getLogin(), user.getEmail(), user.getFirstName(), user.getLastName());
    }
}

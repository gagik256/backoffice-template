package com.gs.template.backoffice.dto.log;

import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.model.entities.elastic.log.ChangedType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PermissionChangeDto {
    private ChangedType changedType;
    private PermissionDto changedPermission;
}

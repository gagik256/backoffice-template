package com.gs.template.backoffice.dto.log;

import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import com.gs.template.backoffice.model.entities.elastic.log.UserLog;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * UserLog dto
 */
@Getter
public class UserLogDto {
    public static final String LOGIN = "login";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String COUNTRY = "country";
    public static final String EMAIL = "email";
    public static final String ROLES = "roles";

    private final LogEventType eventType;
    private final List<ChangedFieldDto> userChangedFields = new ArrayList<>();
    private final long createdAt;

    public UserLogDto(UserLog userLog) {
        this.eventType = userLog.getEventType();
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(LOGIN)
                .oldValue(userLog.getUserChangedFields().getLoginOldValue())
                .newValue(userLog.getUserChangedFields().getLoginNewValue())
                .build());
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(FIRST_NAME)
                .oldValue(userLog.getUserChangedFields().getFirstNameOldValue())
                .newValue(userLog.getUserChangedFields().getFirstNameNewValue())
                .build());
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(LAST_NAME)
                .oldValue(userLog.getUserChangedFields().getLastNameOldValue())
                .newValue(userLog.getUserChangedFields().getLastNameNewValue())
                .build());
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(COUNTRY)
                .oldValue(userLog.getUserChangedFields().getCountryOldValue())
                .newValue(userLog.getUserChangedFields().getCountryNewValue())
                .build());
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(EMAIL)
                .oldValue(userLog.getUserChangedFields().getEmailOldValue())
                .newValue(userLog.getUserChangedFields().getEmailNewValue())
                .build());
        this.userChangedFields.add(ChangedFieldDto.builder()
                .name(ROLES)
                .oldValue(userLog.getUserChangedFields().getRolesOldValue())
                .newValue(userLog.getUserChangedFields().getRolesNewValue())
                .build());
        this.createdAt = userLog.getCreatedAt();
    }
}

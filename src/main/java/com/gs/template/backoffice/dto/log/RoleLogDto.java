package com.gs.template.backoffice.dto.log;

import com.gs.template.backoffice.model.entities.elastic.log.LogEventType;
import com.gs.template.backoffice.model.entities.elastic.log.RoleLog;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * RoleLog dto
 */
@Getter
public class RoleLogDto {
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";

    private final LogEventType eventType;
    private final List<ChangedFieldDto> roleChangedFields = new ArrayList<>();
    private List<UserChangeDto> userChanges;
    private List<PermissionChangeDto> permissionChanges;
    private final long createdAt;

    public RoleLogDto(RoleLog roleLog, List<UserChangeDto> userChanges, List<PermissionChangeDto> permissionChanges) {
        this.eventType = roleLog.getEventType();
        this.roleChangedFields.add(ChangedFieldDto.builder()
                .name(NAME)
                .oldValue(roleLog.getRoleChangedFields().getNameOldValue())
                .newValue(roleLog.getRoleChangedFields().getNameNewValue())
                .build());
        this.roleChangedFields.add(ChangedFieldDto.builder()
                .name(DESCRIPTION)
                .oldValue(roleLog.getRoleChangedFields().getDescriptionOldValue())
                .newValue(roleLog.getRoleChangedFields().getDescriptionNewValue())
                .build());
        this.userChanges = userChanges;
        this.permissionChanges = permissionChanges;
        this.createdAt = roleLog.getCreatedAt();
    }
}

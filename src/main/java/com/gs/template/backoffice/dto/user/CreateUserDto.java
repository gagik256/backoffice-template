package com.gs.template.backoffice.dto.user;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@ToString(exclude = "password")
public class CreateUserDto {
    @NotBlank
    String login;
    @NotBlank
    String password;
    String firstName;
    String lastName;
    @NotBlank
    String email;
    String country;
    Set<Long> roles;
}

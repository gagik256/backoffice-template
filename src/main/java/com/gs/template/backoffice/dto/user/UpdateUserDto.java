package com.gs.template.backoffice.dto.user;

import lombok.Data;

import java.util.Set;

@Data
public class UpdateUserDto {

    String email;
    String firstName;
    String lastName;
    String country;
    Set<Long> roles;
}

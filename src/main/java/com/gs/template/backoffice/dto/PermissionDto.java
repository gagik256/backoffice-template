package com.gs.template.backoffice.dto;

import com.gs.template.backoffice.model.entities.jpa.Permission;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PermissionDto {
    private final String name;
    private final String description;

    public static PermissionDto of(Permission permission) {
        return new PermissionDto(permission.getName(), permission.getDescription());
    }
}

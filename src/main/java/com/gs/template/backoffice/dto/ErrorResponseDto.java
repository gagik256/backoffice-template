package com.gs.template.backoffice.dto;

import com.gs.template.backoffice.model.api.ErrorResponse;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponseDto {
    private final Integer code;
    private final String description;

    public static ErrorResponseDto of(ErrorResponse errorResponse) {
        return new ErrorResponseDto(errorResponse.getCode(), errorResponse.getDescription());
    }
}

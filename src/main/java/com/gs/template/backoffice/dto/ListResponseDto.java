package com.gs.template.backoffice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ListResponseDto<T> {
    List<T> result;

    public static <T> ListResponseDto<T> of(List<T> list) {
        return new ListResponseDto<>(list);
    }
}

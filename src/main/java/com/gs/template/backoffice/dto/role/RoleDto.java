package com.gs.template.backoffice.dto.role;

import com.gs.template.backoffice.dto.PermissionDto;
import com.gs.template.backoffice.dto.user.ShortUserDto;
import com.gs.template.backoffice.model.entities.jpa.Role;
import lombok.Value;

import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Value
public class RoleDto {

    private final long id;
    private final String name;
    private final String description;
    private final Long createdAt;
    private final Long updatedAt;
    private final Set<PermissionDto> permissions;
    private final Set<ShortUserDto> users;

    public static RoleDto of(Role role) {
        return new RoleDto(role.getId(), role.getName(), role.getDescription(), role.getCreatedAt(), role.getUpdatedAt(),
                role.getPermissions().stream().map(PermissionDto::of).collect(toSet()),
                role.getUsers().stream().map(ShortUserDto::of).collect(toSet()));
    }
}

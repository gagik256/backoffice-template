package com.gs.template.backoffice.dto.filter;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterDto {
    private static final Map<String, Operator> OPERATOR_MAP = Arrays.stream(Operator.values())
            .collect(Collectors.toMap(Enum::name, Function.identity()));

    private String field;
    private Operator operator;
    private String value;

    public enum Operator {
        EQ, GE, LE;

        @Nullable
        @JsonCreator
        public static Operator fromString(String key) {
            return OPERATOR_MAP.get(key.toUpperCase());
        }
    }
}

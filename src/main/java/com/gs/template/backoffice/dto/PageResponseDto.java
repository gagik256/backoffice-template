package com.gs.template.backoffice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Data
@AllArgsConstructor
public class PageResponseDto<T> {
    List<T> result;
    Total total;

    @Value
    public static class Total {
        long count;
    }

    public static <T> PageResponseDto<T> of(List<T> list, long total) {
        return new PageResponseDto<>(list, new Total(total));
    }
}

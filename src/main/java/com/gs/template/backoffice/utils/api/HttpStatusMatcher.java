package com.gs.template.backoffice.utils.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import com.gs.template.backoffice.model.api.Error;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpStatusMatcher {

    public static int getStatusCode(Error error) {
        return getStatus(error).value();
    }

    public static HttpStatus getStatus(Error error) {
        switch (error) {
            case NOT_FOUND:
            case USER_NOT_FOUND:
            case ROLE_NOT_FOUND:
                return HttpStatus.NOT_FOUND;
            case FORBIDDEN:
                return HttpStatus.FORBIDDEN;
            case BAD_REQUEST:
            case PERMISSION_NOT_FOUND:
                return HttpStatus.BAD_REQUEST;
            case EMAIL_ALREADY_EXISTS:
            case LOGIN_ALREADY_EXISTS:
            case ROLE_NAME_ALREADY_EXISTS:
            case DEPENDENCIES_ERROR:
                return HttpStatus.CONFLICT;
            case SERVER_ERROR:
                return HttpStatus.INTERNAL_SERVER_ERROR;
            default:
                throw new IllegalArgumentException();
        }
    }
}

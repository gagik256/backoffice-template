package com.gs.template.backoffice.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class SerializationUtil {

    private final ObjectMapper objectMapper;

    /**
     * For deep copying of the object with all nested classes and collections
     * @param object
     * @param clazz
     * @return
     */
    public <T> T deepCopy(T object, Class<T> clazz) {
        T copy = null;
        try {
            copy = objectMapper.readValue(objectMapper.writeValueAsString(object), clazz);
        } catch (JsonProcessingException e) {
            log.error("Deep copy fails, can not convert : {}", object);
        }
        return copy;
    }

    /**
     * For deep copying of set of the object with all nested classes and collections
     * @param objects
     * @param clazz
     * @return
     */
    public <T> Set<T> deepCopySet(Set<T> objects, Class<T> clazz) {
        return objects.stream().map(o -> deepCopy(o, clazz)).collect(Collectors.toSet());
    }

}

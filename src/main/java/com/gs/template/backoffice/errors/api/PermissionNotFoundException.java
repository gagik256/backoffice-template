package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

public class PermissionNotFoundException extends ApiException {

    public PermissionNotFoundException(Long id) {
        super(Error.NOT_FOUND, "Permission with id: " + id + " not found");
    }
}

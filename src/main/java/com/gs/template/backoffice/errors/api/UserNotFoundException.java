package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

import javax.annotation.Nonnull;

public class UserNotFoundException extends ApiException {
    public UserNotFoundException(@Nonnull Long id) {
        super(Error.USER_NOT_FOUND, "User not found by id: " + id);
    }
}

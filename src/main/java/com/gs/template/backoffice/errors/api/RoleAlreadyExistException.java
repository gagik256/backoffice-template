package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

public class RoleAlreadyExistException extends ApiException {

    public RoleAlreadyExistException(String name) {
        super(Error.ROLE_NAME_ALREADY_EXISTS, "Role with name : " + name);
    }
}

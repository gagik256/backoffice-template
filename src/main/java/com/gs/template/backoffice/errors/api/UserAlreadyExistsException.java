package com.gs.template.backoffice.errors.api;

import javax.annotation.Nonnull;
import com.gs.template.backoffice.model.api.Error;

public class UserAlreadyExistsException extends ApiException {

    public UserAlreadyExistsException(Error error, @Nonnull String message) {
        super(error, message);
    }
}

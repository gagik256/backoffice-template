package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

public class UserCreationException extends ApiException {

    public UserCreationException(Error error, String message) {
        super(error, message);
    }
}

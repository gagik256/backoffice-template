package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

public class RoleNotFoundException extends ApiException {

    public RoleNotFoundException(Long id){
        super(Error.ROLE_NOT_FOUND, "Role with id : " + id);
    }
}

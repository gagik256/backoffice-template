package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.Error;

public class RoleCreationException extends ApiException {

    public RoleCreationException(Error error, String message) {
        super(error, message);
    }
}

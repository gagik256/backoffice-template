package com.gs.template.backoffice.errors.api;

import com.gs.template.backoffice.model.api.ErrorResponse;
import com.gs.template.backoffice.utils.api.HttpStatusMatcher;
import com.gs.template.backoffice.model.api.Error;
import lombok.Getter;

@Getter
public abstract class ApiException extends Exception {

    private final ErrorResponse error;
    private final int status;

    public ApiException(Error error, String message) {
        super(message);
        this.error = new ErrorResponse(error.getCode(), error.getDescription());
        this.status = HttpStatusMatcher.getStatusCode(error);
    }

    public ApiException(Error error, String message, Throwable cause) {
        super(message, cause);
        this.error = new ErrorResponse(error.getCode(), error.getDescription());
        this.status = HttpStatusMatcher.getStatusCode(error);
    }
}

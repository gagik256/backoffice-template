package com.gs.template.backoffice.errors.services;

public class IllegalEnumNameException extends RuntimeException {
    public <T extends Enum<T>> IllegalEnumNameException(Class<T> enumClass, String message, Throwable cause) {
        super(String.format("Illegal filter field value : %s , enum type : %s ", message, enumClass), cause);
        System.out.println(enumClass);
    }
}
